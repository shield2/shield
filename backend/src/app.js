const express = require('express')
const mongoose = require('mongoose');
const { isNil } = require('ramda')
const cors = require('cors')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const axios = require('axios')
const path = require('path');
require('dotenv').config();
global.mongoose = mongoose;

const connectDb = () => {
    try {
        mongoose.connect(process.env.MONGO, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log("connected to mongo db")
    } catch (error) {
        handleError(error);
    }
}

const app = express()
const port = process.env.PORT

console.log(process.env.HOST);

app.use(cors({ credentials: true, origin: `${process.env.HOST}` }));
app.use(bodyParser.json())
app.use(cookieParser())
connectDb();

// app.use('/', async (req, res, next) => {
//     if (req.cookies && req.cookies[cookieName]) {
//         const user = await extractUser(req.cookies[cookieName]);
//         if (user) {
//             next;
//         } else {
//             res.redirect('http://localhost:3000/login');
//         }
//     } else {
//         res.redirect('http://localhost:3000/login');
//     }
// })
// app.use('/api', express.static('build'))
require('./routes/login.js')(app)
require('./routes/users.js')(app)
require('./routes/courses.js')(app)
require('./routes/defenses.js')(app)
require('./routes/registers.js')(app)

// if (process.env.NODE_ENV === 'production') {
    // Serve any static files
    app.use(express.static(path.join(__dirname, '../build')));

    // Handle React routing, return all requests to React app
    app.get('*', function (req, res) {
        res.sendFile(path.join(__dirname, '../build', 'index.html'));
    });
// }

app.listen(port, () => console.log(`Example app listening on port ${port}!`))