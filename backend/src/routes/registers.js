var RegistersFactory = require('../mongo/register.factory.js');
// const bodyParser = require('body-parser');
// Opens App Routes
module.exports = function (app) {
   
   // app.use(bodyParser);
   /** Getting all the registers of defense **/
   app.get('/api/getDefenseRegisters', function (req, res) {
      // console.log("get registers of defense")
      const data = req.query.id;
      // console.log(data);
      RegistersFactory.getDefenseRegisters(res, data).then(function (registers) {
         res.json(registers);
      }, function (error) {
         res.json(error);
         // console.log(error)
      });
   });

   app.post('/api/getDefenseRegisterById', function (req, res) {
      RegistersFactory.getDefenseRegisterById(res, req.body).then(function (registers) {
         res.json(registers);
      }, function (error) {
         res.json(error);
         // console.log(error)
      });
   });
   
   app.post('/api/getDefenseRegistersStudents', (req, res) => {
      RegistersFactory.getDefenseRegistersStudents(res, req.body).then(function (registers) {
         res.json(registers);
      }, function (error) {
         res.json(error);
         // console.log(error)
      });
   });

   app.post('/api/register', function (req, res) {
      RegistersFactory.register(res, req.body).then(function (registers) {
         res.json(registers);
      }, function (error) {
         res.json(error);
         // console.log(error)
      });
   });

   app.post('/api/getMyDefenses', function (req, res) {
      RegistersFactory.getMyDefenses(res, req.body).then(function (getMyDefenses) {
         res.json(getMyDefenses);
      }, function (error) {
         res.json(error);
         // console.log(error)
      });
   });


   app.post("/api/deleteUserFromRegister", (req, res) => {
      const data = req.body;
      // console.log(data);
      RegistersFactory.deleteUserFromRegister(res, data);
    });

    app.post("/api/removeAllStudents", (req, res) => {
      const data = req.body;
      RegistersFactory.deleteAllStudentsRegister(res, data);
    });
}


//deleteUserFromRegister

