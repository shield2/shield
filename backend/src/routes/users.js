var UserFactory = require("../mongo/user.factory.js");

// Opens App Routes
module.exports = function (app) {
  /** Getting all the users **/
  app.get("/api/allUsers", function (req, res) {
    UserFactory.getUsers().then(
      function (users) {
        res.json(users);
      },
      function (error) {
        res.json(error);
      }
    );
  });

  app.post("/api/addUser", (req, res) => {
    const data = req.body;
    UserFactory.addUser(res, data);
  });

  app.post("/api/deleteUser", (req, res) => {
    const data = req.body;
    UserFactory.deleteUser(res, data);
  });

  app.post("/api/updateUser", (req, res) => {
    const data = req.body;
    UserFactory.updateUser(res, data);
  });

  
   /** Get user details by _id**/
   app.get('/api/getUserById', function (req, res) {
    const data = req.query.id;
    // console.log("get details of user")
    // console.log(data);
    UserFactory.getUserById(res, data)
 })
};
