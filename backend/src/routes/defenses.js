var DefenseFactory = require('../mongo/defense.factory.js');

// Opens App Routes
module.exports = function (app) {

   /** Getting all the defenses of course **/
   app.get('/api/getDefenses', function (req, res) {
      // console.log("get defenses of course")
       const data = req.query.id;
      //  console.log(data);
       DefenseFactory.getDefenses(res, data).then(function (defenses) {
         res.json(defenses);
      }, function (error) {
         res.json(error);
         // console.log(error)
      });
   });

   /** Add new defense **/
   app.post('/api/addDefense', (req, res) => {
      const data = req.body;
      DefenseFactory.addDefense(res, data)
   })

   /** Delete course **/
   app.post('/api/deleteDefense', (req, res) => {
      const data = req.body.id;
      DefenseFactory.deleteDefense(res, data)
   })


   /** Update defense details **/
   app.post('/api/updateDefense', (req, res) => {
      const data = req.body;
      DefenseFactory.updateDefense(res, data)
   })

   app.post('/api/userDefenses', (req, res) => {
      var userId = req.body.uId
      DefenseFactory.getAllDefenses(res,userId)
   })

   app.post('/api/getDefenseById', (req, res) => {
      DefenseFactory.getDefenseById(res, req.body)
  })
  
}