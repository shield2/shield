module.exports = function (app) {

    const jwt = require('jsonwebtoken')
    const cookieName = 'shield.token'

    app.post('/api/login', async (req, res) => {
        const data = req.body;
        var getUser = await checkAuth(data.id, data.username)
        if (getUser != false) {
            // console.log(getUser)
            res.cookie(cookieName, generateCookie(getUser.id, `${getUser.firstName} ${getUser.lastName}`, getUser.isAdmin))
            // console.log(getUser.firstName + " connected")
            // console.log("hanuni connected")
            res.json(getUser)
            return;
        } else {
            res.status(403).end();
        }
    })

    app.get('/api/getUser', async (req, res) => {
        if (req.cookies && req.cookies[cookieName]) {
            const user = await extractUser(req.cookies[cookieName]);
            if (user) {
                req.user = user;
                res.json(req.user)
                return;
            }
        }
        res.status(403).end();
    })

    const generateCookie = (id, name, admin) => {
        return (jwt.sign({
            id: id,
            userName: name,
            isAdmin: admin
        }, 'shield', { expiresIn: '12h' }))
    }

    const extractUser = (token) => {
        const decoded = jwt.verify(token, 'shield', function (err, decoded) {
            if (err) {
                // console.log(err.message)
                return null;
            } else {
                // console.log(decoded.id + ":" + decoded.userName)
                return decoded;
            }
        });
        return decoded
    }

    const checkAuth = (id, name) => {
        return require('../mongo/user.factory').isUserExist(id, name)
    }

}