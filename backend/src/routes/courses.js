var CourseFactory = require('../mongo/course.factory.js');

// Opens App Routes
module.exports = function (app) {

   /** Getting all the courses of lecturer **/
   app.get('/api/allCoursesOfLecturer', function (req, res) {
      const data = req.query.id;
      CourseFactory.allCoursesOfLecturer(res, data).then(function (courses) {
         res.json(courses);
      }, function (error) {
         res.json(error);
         // console.log(error)
      });
   });

   /** Add new course **/
   app.post('/api/addCourse', (req, res) => {
      const data = req.body;
      // console.log(data)
      CourseFactory.addCourse(res, data)
   })

   /** Delete course **/
   app.post('/api/deleteCourse', (req, res) => {
      const data = req.body.id;
      CourseFactory.deleteCourse(res, data)
   })

   /** Get course details by _id**/
   app.get('/api/getCourseByID', function (req, res) {
      // console.log("get Course details")
      const data = req.query.id;
      // console.log(data);
      CourseFactory.getCourseByID(res, data)
   })

   /** Update course details **/
   app.post('/api/updateCourse', (req, res) => {
      const data = req.body;
      CourseFactory.updateCourse(res, data)
   })

   //getCoursesOfStudent
   app.post('/api/getCoursesOfStudent', (req, res) => {
      const data = req.body;
      const userId = data.uId
      CourseFactory.getCoursesOfStudent(res, userId)
   })

   //add new student to course
   app.post('/api/addStudentToCourse', (req, res) => {
      // console.log("add student to course " + req.body.id + " studentId " + req.body.studentId )
      const data = req.body;
      CourseFactory.addStudentToCourse(res, data)
   })

   app.get('/api/hanuni', (req, res) => {
      require('../mongo/schema').DefenseRegisters.find({}).exec(function (err, docs) { res.send(docs) })
   });
}