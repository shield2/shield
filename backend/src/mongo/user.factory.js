var Users = require("./schema.js").Users;
var Courses = require("./schema.js").Courses;

exports.getUsers = () => {
  return new Promise(function (resolve, reject) {
    Users.find({}).exec(function (err, users) {
      if (err) {
        return reject({ err: "Error while fetching users" });
      }
      // If no errors are found, it responds with a JSON of all users
      return resolve(users);
    });
  });
};

exports.getUserById = (res, data) => {
  Users.findOne({ id: data }).exec(function (err, docs) {
    res.send(docs);
  });
};
exports.isUserExist = async (id, name) => {
    var exist = await Users.findOne({ id: id, firstName: name })
    if (exist == null) {
        return false
    }
    return exist.toJSON();
}

exports.addUser = (res, data) => {
  var newUser = new Users({
    firstName: data.firstName,
    lastName: data.lastName,
    id: data.id,
    isAdmin: data.isAdmin,
    mail: data.mail,
  });

  newUser.save(function (err, u) {
    if (err && err.code == 11000) {
      // console.error(err);
      res.status(400).send({ err: "משתמש עם תעודת זהות זו כבר קיים." });
    } else {
      // console.log(u.id + " saved to Users collection.");
      res.send();
    }
  });
};


exports.deleteUser = (res, data) => {

  Courses.find({ students: Number(data.id) }).select('id').exec(function (err, courses) {
    if (courses.length === 0)
    {
      Users.find({ id: data.id }).remove().exec();
      res.send() 
    } else {
      res.status("403").send("User is signed to courses")
    }
  }) 
};

exports.updateUser = (res, data) => {
  //console.log(data)
  var userId = { id: data.id };
  var detailsToChange = {
    firstName: data.firstName,
    lastName: data.lastName,
    isAdmin: data.isAdmin,
    mail: data.mail
  };

  Users.findOneAndUpdate(userId, detailsToChange, function (err, u) {
    if (err) return res.send(500, { error: err });
    res.send(200);
    // console.log(u.id + " Updated in Users collection.");
  });
};
