var DefenseRegisters = require("./schema.js").DefenseRegisters;
var Courses = require("./schema.js").Courses;
var Users = require("./schema.js").Users;
const nodemailer = require("nodemailer");

exports.getDefenseRegisters = (res, data) => {
  return new Promise(function (resolve, reject) {
    DefenseRegisters.find({ defenseId: data }).exec(function (err, registers) {
      if (err) {
        return reject({ err: "Error while fetching defenses" });
      }
      // If no errors are found, it responds with a JSON of all registers
      return resolve(registers);
    });
  });
};

exports.getDefenseRegisterById = (res, data) => {
  return new Promise(function (resolve, reject) {
    DefenseRegisters.find({
      defenseId: data.defenseId,
      groupMembers: { $in: [[], data.groupMembers] },
    })
      .select("hour")
      .exec(function (err, docs) {
        if (err) {
          return reject({ err: "Error while getting a defenseRegister" });
        }
        return resolve(docs);
      });
  });
};

exports.getDefenseRegistersStudents = (res, data) => {
  return new Promise(function (resolve, reject) {
    DefenseRegisters.find({
      defenseId: data.defenseId,
      hour: { $ne: data.hour },
    })
      .select("groupMembers") //, hour: { $ne: data.hour } ==> if we can register himself with other people
      .exec(function (err, docs) {
        if (err) {
          return reject({ err: "Error while getting a defense members" });
        }
        return resolve(docs);
      });
  });
};

exports.register = (res, data) => {
  return new Promise(function (resolve, reject) {
    var defenseRegister = {
      defenseId: data.defenseId,
      hour: data.hour,
      groupMembers: [],
    };
    var detailsToChange = {
      groupMembers: data.students,
    };

    DefenseRegisters.updateOne(
      defenseRegister, // Query parameter
      {
        // Update document
        $set: detailsToChange,
      }
    ).exec(function (err, u) {
      if (err) return reject({ err: err });
      return resolve(u);
    });
  });
};

exports.getMyDefenses = (res, data) => {
  return new Promise(function (resolve, reject) {
    DefenseRegisters.aggregate([
      { $match: { groupMembers: data.userId } },
      {
        $project: {
          defenseId: {
            $toObjectId: "$defenseId",
          },
          date: 1,
          hour: 1,
          groupMembers: 1,
        },
      },
      {
        $lookup: {
          from: "Defenses",
          localField: "defenseId",
          foreignField: "_id",
          as: "defense_properties",
        },
      },
      {
        $replaceRoot: {
          newRoot: {
            $mergeObjects: [
              { $arrayElemAt: ["$defense_properties", 0] },
              "$$ROOT",
            ],
          },
        },
      },
      { $project: { defense_properties: 0 } },
      {
        $addFields: {
          courseId: {
            $toObjectId: "$courseId",
          },
        },
      },
      {
        $lookup: {
          from: "Courses",
          localField: "courseId",
          foreignField: "_id",
          as: "course_properties",
        },
      },
      {
        $addFields: {
          course_properties: { $arrayElemAt: ["$course_properties", 0] },
        },
      },
    ]).exec((err, defenses) => {
      if (err) {
        return reject({ err: err });
      } else {
        return resolve(defenses);
      }
    });
  });
};

findCourse = (id) => {
  Courses.findById(id)
    .exec()
    .then((audiences) => {
      return audiences;
    });
};

exports.deleteUserFromRegister = (res, data) => {
  //console.log(data)
  var defenseRegister = {
    defenseId: data.defId,
    hour: data.hour,
  };
  var detailsToChange = {
    groupMembers: data.groupMembers,
  };

  DefenseRegisters.updateOne(
    defenseRegister, // Query parameter
    {
      // Update document
      $set: detailsToChange,
    }
    // { upsert: true } // Options
  ).exec(function (err, u) {
    if (err) return res.send(500, { error: err });
    res.send(200);
  });
};

exports.deleteAllStudentsRegister = (res, data) => {
  var defenseRegister = {
    defenseId: data.defenseId,
    hour: data.hour,
  };

  var detailsToChange = {
    groupMembers: [],
  };

  var mailDetails = {
    userId: data.userId,
    groupMembers: data.groupMembers,
    courseName: data.courseName,
  };

  var isEdit = data.isEdit

  DefenseRegisters.updateOne(defenseRegister, {
    $set: detailsToChange,
  }).exec(function (err, u) {
    if (err) return res.send(500, { error: err });
    if (!isEdit) {
      deleteGroupMail(
        mailDetails.userId,
        mailDetails.groupMembers,
        mailDetails.courseName
      );
      // console.log("User: " + mailDetails.userId + "\n Deleted: " + mailDetails.groupMembers);
    } else {
      editGroupMail(
        mailDetails.userId,
        mailDetails.groupMembers,
        mailDetails.courseName
      );
    }
    res.send(200);
  });
};

const deleteGroupMail = (userId, groupMembers, courseName) => {
  var mailAddresses = [];
  Users.find({ id: { $in: groupMembers } })
    .select("mail")
    .exec(function (err, adresses) {
      if (!err) {
        adresses.forEach((address) => {
          mailAddresses.push(address["mail"]);
        });
        Users.findOne({ id: userId }).exec(function (err, u) {
          // console.log(mailAddresses);
          // console.log(u);
          // console.log(courseName);

          let mailText =
            ",לידיעתך \nהמשתמש " +
            u.firstName +
            " " +
            u.lastName +
            " הסיר את הקבוצה שהיית חבר בה מרישום להגנה בקורס " +
            courseName + ". \n \n" +
            "זהו מייל אוטומטי ממערכת Shield \n" +
            "אין להשיב עליו"
          let mailTransporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
              user: "Cheers170296@gmail.com",
              pass: "Cheers290496",
            },
          });

          let mailDetails = {
            from: "Cheers170296@gmail.com",
            to: mailAddresses,
            subject: "הודעה: מחיקת רישום להגנה",
            text: mailText,
          };

          mailTransporter.sendMail(mailDetails, function (err, data) {
            if (err) {
              // console.log("Error Occurs");
            } else {
              // console.log("Email sent successfully");
            }
          });
        });
      }
    });
};

editGroupMail = (userId, groupMembers, courseName) => {
  var mailAddresses = [];
  Users.find({ id: { $in: groupMembers } })
    .select("mail")
    .exec(function (err, adresses) {
      if (!err) {
        adresses.forEach((address) => {
          mailAddresses.push(address["mail"]);
        });
        Users.findOne({ id: userId }).exec(function (err, u) {
          // console.log(mailAddresses);
          // console.log(u);
          // console.log(courseName);

          let mailText =
            ",לידיעתך \nהמשתמש " +
            u.firstName +
            " " +
            u.lastName +
            " ערך את רישום ההגנה של הקבוצה שהיית חבר/ה בה, פרטי ההגנה (לרבות שעה/תאריך/חברי הקבוצה) שונו " +
            courseName + ". \n \n" +
            "זהו מייל אוטומטי ממערכת Shield \n" +
            "אין להשיב עליו"
          let mailTransporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
              user: "Cheers170296@gmail.com",
              pass: "Cheers290496",
            },
          });

          let mailDetails = {
            from: "Cheers170296@gmail.com",
            to: mailAddresses,
            subject: "הודעה: עריכת רישום להגנה",
            text: mailText,
          };

          mailTransporter.sendMail(mailDetails, function (err, data) {
            if (err) {
              // console.log("Error Occurs");
            } else {
              // console.log("Email sent successfully");
            }
          });
        });
      }
    });
};