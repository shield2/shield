
// ------------------ NOT_IN_USE!! ------------------

var Schema = mongoose.Schema;

// ------------------ NOT_IN_USE!! ------------------

var Users = mongoose.model('Users', new Schema({
    id: {type: String, unique: true},
    firstName: String,
    lastName: String,
    isAdmin: Boolean,
    city: String
}), 'Users');

// ------------------ NOT_IN_USE!! ------------------

var Courses = mongoose.model('Courses', new Schema({
    id: Number,
    name: String,
    lecturerID: Number,
    lecturer: String,
    semester: String,
    year: String,
    students: Array, //Students IDs
    description: String,
    defenses: [{
        _id: String,
        date: String,
        startTime: String,
        endTime: String, 
        duration: Number,
        minStudents: Number,
        maxStudents: Number,
        groups: [{
            hour: String,
            groupMembers: Array //Students IDs
        }]
    }]
}), 'Courses');

// ------------------ NOT_IN_USE!! ------------------

module.exports = {
    Users: Users,
    Courses: Courses
}