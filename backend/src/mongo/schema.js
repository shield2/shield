var Schema = mongoose.Schema;

var Users = mongoose.model('Users', new Schema({
    id: {type: String, unique: true},
    firstName: String,
    lastName: String,
    isAdmin: Boolean,
    mail: String
}), 'Users');

var Courses = mongoose.model('Courses', new Schema({
    id: Number,
    name: String,
    lecturerID: Number,
    lecturer: String,
    semester: String,
    year: String,
    minStudents: Number,
    maxStudents: Number,
    numOfStudents: Number,
    description: String,
    students: Array,
}), 'Courses');

var Defenses = mongoose.model('Defenses',new Schema({
    courseId: String,
    date: String,
    time: Number,
    startTime: String,
    endTime: String, 
}), 'Defenses');

var DefenseRegisters = mongoose.model('DefenseRegisters',new Schema({
    defenseId: String,
    date: String,
    hour: Number,
    groupMembers: Array
}), 'DefenseRegisters');

module.exports = {
    Users: Users,
    Courses: Courses,
    Defenses: Defenses,
    DefenseRegisters: DefenseRegisters
}