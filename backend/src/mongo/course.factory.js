var Courses = require('./schema.js').Courses;
var Defenses = require("./schema.js").Defenses;
var Users = require('./schema.js').Users;
var DefenseRegisters = require("./schema.js").DefenseRegisters;
var DefenseFactory = require('../mongo/defense.factory.js');

exports.allCoursesOfLecturer = (res, data) => {
    return new Promise(function (resolve, reject) {
        Courses.find({ lecturerID: data })
            .exec(function (err, courses) {
                if (err) {
                    return reject({ err: 'Error while fetching courses' });
                }
                // If no errors are found, it responds with a JSON of all courses
                return resolve(courses);
            });
    });
}

exports.addCourse = (res, data) => {

    var stList = []

    // console.log("students list : " + data.studentsList)

    if (data.studentsList != "")
    {
        Users.insertMany(data.studentsList, {ordered: false}, function (err, u) {
            if (err) {
                console.error(err);
            } else {
                console.log("Students has been saved successfully");
                res.send();
            }
        });
    
        data.studentsList.forEach(st => {
            stList.push(st["id"])
        });
    }

    var newCourse = new Courses({
        name: data.courseName,
        lecturerID: data.lecturerID,
        lecturer: data.lecturer,
        semester: data.semester,
        year: data.year,
        minStudents: data.minStudents,
        maxStudents: data.maxStudents,
        description: data.about,
        students: stList
    })

    newCourse.save(function (err, u) {
        if (err) {
            console.error(err);
        } else {
            // console.log(u.name + " saved to Courses collection.");
            res.send();
        }
    });
}

exports.deleteCourse = (res, data) => {
    console.log("course id to delete : " + data)
    Courses.find({ _id: data }).deleteOne().exec()
    DefenseFactory.getDefenses(res, data).then(function (defenses) {
        // console.log("defenses to delete:" + defenses)

        // for(var defense in defenses) {
        //     DefenseRegisters.deleteMany({defenseId: defense._id}).exec()
        //  }
        defenses.forEach(defense => {
            // console.log("defense id : " + defense._id.toString())
            DefenseRegisters.deleteMany({ defenseId: defense._id.toString() }).exec()
        });

    })

    Defenses.deleteMany({ courseId: data }).exec();
    res.send();
}

exports.getCourseByID = (res, data) => {
    Courses.findById(data).exec(function (err, docs) { res.send(docs) })
}

exports.updateCourse = (res, data) => {

    var courseID = { _id: data.id }
    var detailsToChange = {
        name: data.courseName,
        semester: data.semester,
        year: data.year,
        minStudents: data.minStudents,
        maxStudents: data.maxStudents,
        description: data.about
    }

    if(data.studentsList != "")
    {
        Users.insertMany(data.studentsList, function (err, u) {
            if (err) {
                console.error(err);
            } else {
                // console.log("Students has been saved successfully");
                res.send();
            }
        });
    
        var stList = []
        data.studentsList.forEach(st => {
            stList.push(st["id"])
        });

        detailsToChange = {...detailsToChange, students: stList}
    }

    Courses.findOneAndUpdate(courseID, detailsToChange, function (err, u) {
        if (err) return res.status(500).send({ error: err });
        res.send(200)
        // console.log(u.name + " Updated in Courses collection.");
    });

}

exports.getCoursesOfStudent = (res, uId) => {
    Courses.find({ students: Number(uId) }).select('id').exec(function (err, docs) { res.send(docs) })
}

exports.addStudentToCourse = (res, data) => {
    var courseID = { _id: data.id }
    Courses.findOneAndUpdate({_id: data.id}, {$push: {students: Number(data.studentId)}}, function (err, docs) {
        if (err) {
            console.error(err);
        } else {
            // console.log("Student has been saved successfully");
            res.send();
        }
    });
}


