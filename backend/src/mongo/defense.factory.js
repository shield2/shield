var Defenses = require("./schema.js").Defenses;
var Courses = require("./schema.js").Courses;
var Users = require("./schema.js").Users;
const nodemailer = require("nodemailer");

var DefenseRegisters = require("./schema.js").DefenseRegisters;

exports.getDefenses = (res, data) => {
  return new Promise(function (resolve, reject) {
    Defenses.find({ courseId: data }).exec(function (err, defenses) {
      if (err) {
        return reject({ err: "Error while fetching defenses" });
      }
      // If no errors are found, it responds with a JSON of all defenses
      return resolve(defenses);
    });
  });
};

function hourString(date) {
  var hours = date.getHours();
  var mins = date.getMinutes();
  if (hours < 10) hours = "0" + hours;
  if (mins < 10) mins = "0" + mins;

  return hours + ":" + mins;
}

exports.addDefense = (res, data) => {
  var newDefense = new Defenses({
    courseId: data.courseId,
    date: data.date,
    time: data.time,
    startTime: data.startTime,
    endTime: data.endTime,
  });

  newDefense.save(function (err, u) {
    if (err) {
      console.error(err);
    } else {
      // console.log(u.date + " saved to Defenses collection.");

      var interval = newDefense.time;
      var start = new Date("01/01/1970" + " " + newDefense.startTime);
      var endTime = new Date("01/01/1970" + " " + newDefense.endTime);
      var end = new Date(start.getTime() + interval * 60000);
      // console.log(end);

      var groupNumber = 1;

      while (end <= endTime) {
        var newRegister = new DefenseRegisters({
          defenseId: u._id,
          date: newDefense.date,
          //hour: hourString(start),
          hour: groupNumber,
          groupMembers: [],
        });
        // console.log(newRegister);

        newRegister.save(function (err, dr) {
          if (err) return console.error(err);
          // console.log(dr.hour + " saved to DefenseRegister collection.");
        });

        var start = end;
        var end = new Date(start.getTime() + interval * 60000);
        groupNumber++;
      }

      res.send();
    }
  });
};

exports.deleteDefense = (res, data) => {
  Defenses.find({ _id: data }).deleteOne().exec();

  DefenseRegisters.find({ defenseId: data}).exec(function (err, registers) {
    if (err) {
      return reject({ err: "Error while fetching defenses" });
    }
    registers.forEach((register) => {
      if (register.groupMembers.length > 0) {
        var mailText2 =
        "לידיעתך, <br> בוטל מועד הגנה אליו נרשמת. <br> אנא בצע רישום מחודש.";
        deleteGroupMail(
          register.groupMembers,
          mailText2,
          "ביטול מועד הגנה"
        );
      }

      DefenseRegisters.deleteMany({ defenseId: data }).exec();
      res.send();
    });
  });
};

exports.updateDefense = (res, data) => {
  // console.log(data);
  var defenseID = { _id: data.id };
  var detailsToChange = {
    date: data.date,
    time: data.time,
    startTime: data.startTime,
    endTime: data.endTime,
  };
  var isStartTimeChanged = false;
  var isDateChanged = false;

  // console.log(data.oldStartTime + " --- " + data.startTime);
  // console.log(data.oldDate + " --- " + data.date);
  if (data.oldStartTime !== data.startTime) {
    isStartTimeChanged = true;
  }
  if (data.date !== data.oldDate) {
    isDateChanged = true;
  }

  DefenseRegisters.find({ defenseId: data.id }).count(function (err, defenses) {
    if (err) {
      return reject({ err: "Error while fetching defense" });
    }

    var groups = defenses;
    // console.log(groups + " defense registers before update");

    //Checks how many defense registers before changes
    // var interval = defense.time;
    // var start = new Date("01/01/1970" + " " + defense.startTime);
    // var endTime = new Date("01/01/1970" + " " + defense.endTime);
    // var end = new Date(start.getTime() + interval * 60000);

    // var groupNumber = 1;

    // while (end <= endTime)
    // {
    //   var start = end;
    //   var end = new Date(start.getTime() + interval * 60000);
    //   groupNumber++;
    // }

    //Checks how many defense registers after changes
    var interval = data.time;
    var start = new Date("01/01/1970" + " " + data.startTime);
    var endTime = new Date("01/01/1970" + " " + data.endTime);
    var end = new Date(start.getTime() + interval * 60000);
    var newGroups = 1;

    while (end <= endTime) {
      var start = end;
      var end = new Date(start.getTime() + interval * 60000);
      newGroups++;
    }

    newGroups--;
    groups++;

    //Checks if need to add more defense registers
    while (groups <= newGroups) {
      var newRegister = new DefenseRegisters({
        defenseId: data.id,
        date: data.date,
        hour: groups,
        groupMembers: [],
      });

      newRegister.save(function (err, dr) {
        if (err) return console.error(err);
        // console.log(dr.hour + " saved to DefenseRegister collection.");
      });

      groups++;
    }

    groups--;

    // Remove registers that out of time
    if (groups > newGroups) {
      // console.log(groups);
      // console.log(newGroups);
      // DefenseRegisters.find({ defenseId: data.id, hour: {$gt : newGroups} }).exec(function (err, registers) {
      //     if (err) {
      //       return reject({ err: "Error while fetching defense" });
      //     }

      //       console.log(registers);
      //   }
      // );

      mailText =
        "לידיעתך, <br> בוצע שינוי בשעות מועד הגנה אליה נרשמת ורישומך נמחק. <br> אנא בצע רישום מחודש למועד המעודכן.";

      DefenseRegisters.find({
        defenseId: data.id,
        hour: { $gt: newGroups },
      }).exec(function (err, registers) {
        if (err) {
          return reject({ err: "Error while fetching defenses" });
        }

        registers.forEach((register) => {
          // console.log("line 171: " + register.groupMembers);
          if (register.groupMembers.length > 0) {
            deleteGroupMail(
              register.groupMembers,
              mailText,
              "מחיקת רישום להגנה עקב שינוי שעות"
            );
          }
        });
      });

      DefenseRegisters.deleteMany({
        defenseId: data.id,
        hour: { $gt: newGroups },
      }).exec();
    }
  });

  //Update date for all defense registers
  DefenseRegisters.find({ defenseId: data.id }).exec(function (err, registers) {
    if (err) {
      return reject({ err: "Error while fetching defenses" });
    }
    // console.log("isStartTimeChanged- " + isStartTimeChanged);
    // console.log("isDateChanged- " + isDateChanged);
    registers.forEach((register) => {
      var registerID = { _id: register._id.toString() };

      var detailsToChange = {
        _id: register._id,
        groupMembers: register.groupMembers,
        defenseId: register.defenseId,
        date: data.date,
        hour: register.hour,
      };

      if (register.groupMembers.length > 0) {
        if (isStartTimeChanged) {
          var mailText1 =
            "לידיעתך, <br> בוצע שינוי בשעות תחילת מועד הגנה אליה נרשמת .<br> אנא התעדכן בשעה החדשה.";
          deleteGroupMail(
            register.groupMembers,
            mailText1,
            "שינוי בשעת תחילת מועד הגנה"
          );
        }
        if (isDateChanged) {
          var mailText2 =
            "לידיעתך, <br> בוצע שינוי בתאריך מועד הגנה אליה נרשמת .<br> אנא התעדכן בתאריך החדש.";
          deleteGroupMail(
            register.groupMembers,
            mailText2,
            "שינוי בתאריך מועד הגנה"
          );
        }
      }

      DefenseRegisters.findOneAndUpdate(registerID, detailsToChange).exec();
    });
  });

  //Update defense details
  Defenses.findOneAndUpdate(defenseID, detailsToChange, function (err, u) {
    if (err) return res.status(500).send({ error: err });
    res.send(200);
    // console.log(u.date + " Updated in defenses collection.");
  });
};

exports.getAllDefenses = (res, uId) => {
  // console.log("getUserDefenses");
  // console.log(uId);
  // get all courses id for user with find and match - write that func in course factory becaue i'll need it
  // get all defenses of that course id with find and in
  Defenses.aggregate([
    {
      $project: {
        courseId: {
          $toObjectId: "$courseId",
        },
        date: 1,
        startTime: 1,
        endTime: 1,
        time: 1
      },
    },
    {
      $lookup: {
        from: "Courses",
        localField: "courseId",
        foreignField: "_id",
        as: "course_properties",
      },
    },
    {
      $replaceRoot: {
        newRoot: {
          $mergeObjects: [
            { $arrayElemAt: ["$course_properties", 0] },
            "$$ROOT",
          ],
        },
      },
    },
    { $project: { course_properties: 0 } },
  ]).exec(function (err, defenses) {
    if (err) {
      res.send(err);
    } else {
      // console.log(defenses);
      Courses.find({
        $or: [{ students: Number(uId) }, { lecturerID: Number(uId) }],
      })
        .select("id")
        .exec(function (err, courses) {
          var coursesIds = courses.map((c) => String(c._id));
          // console.log(coursesIds);
          if (err) return res.status(403).send(err);
          var userDefenses = defenses.filter((x) =>
            coursesIds.includes(String(x.courseId))
          );
          res.send(userDefenses);
        });
      // res.send(defenses);
    }
  });
};

const deleteGroupMail = (groupMembers, text, subject) => {
  // console.log("line 265: " + groupMembers);
  var mailAddresses = [];
  Users.find({ id: { $in: groupMembers } })
    .select("mail")
    .exec(function (err, adresses) {
      if (!err) {
        adresses.forEach((address) => {
          mailAddresses.push(address["mail"]);
        });

        // console.log(mailAddresses);

        let mailTransporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: "Cheers170296@gmail.com",
            pass: "Cheers290496",
          },
        });

        let mailDetails = {
          from: "Cheers170296@gmail.com",
          to: mailAddresses,
          subject: subject,
          html: "<div style='direction: rtl;'> " + text + "</div>",
        };

        mailTransporter.sendMail(mailDetails, function (err, data) {
          if (err) {
            // console.log("Error Occurs");
          } else {
            // console.log("Email sent successfully");
          }
        });
      }
    });
};
