import React from "react";
import { styles } from "./MyDefenses.css.js";
import {
  withStyles,
  Card,
  CardContent,
  Typography,
  CardActions,
  Grid,
  Fab,
} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import Dialog from "@material-ui/core/Dialog";
import axios from "axios";
import DeleteDefenseDialog from "../DefenseRegister/DeleteDefense.js";
import Edit from "../DefenseRegister/Edit"

class MyDefenses extends React.Component {
  //const [open, setOpen] = useState(false);

  constructor(props) {
    super(props);
    this.state = {
      DeleteDialogOpen: false, // delete defense dialog open state
      editDialogOpen: false, // edit defense dialog open state
      registerToDelete: "",
      defenseId: "",
      hour: "",
      userId: this.props.userId,
      myDefenses: [],
      minStudents: "",
      numOfstudents: "",
      groupMembers: "",
      currentRegister: "",
      courseName: ""
    };
  }

  componentDidMount() {
    this.getAllUserDefenses();
  }

  componentDidUpdate(prevProps) {
    if (this.state.userId === "") {
      this.setState({ userId: this.props.userId }, this.getAllUserDefenses)
    }
  }

  componentWillUpdate() {
    if (this.state.userId === "") {
      this.setState({ userId: this.props.userId }, this.getAllUserDefenses)
    }
  }
  getAllUserDefenses = () => {
    // console.log(this.state.userId)
    axios
      .post(
        `/api/getMyDefenses`,
        { userId: this.state.userId },
        { withCredentials: true }
      )
      .then((res) => {
        var myDefs = [];
        var defenses = res.data;
        // console.log(defenses);
        if (!defenses.err) {
          defenses.forEach((def) => {
            myDefs.push({
              regId: def._id,
              defenseId: def.defenseId,
              courseName: def.course_properties.name,
              date: def.date,
              startTime: def.startTime,
              endTime: def.endTime,
              hour: def.hour,
              lecturer: def.course_properties.lecturer,
              minStudents: def.course_properties.minStudents,
              maxStudents: def.course_properties.maxStudents,
              students: def.groupMembers,
              numOfStudents: def.groupMembers.size,
              currentObj: def
            });
          });
        }
        this.setState({ myDefenses: myDefs });
      });
  };


  handleEditOpen = () => {
    this.setState({ editDialogOpen: true })
  };

  handleEditClose = () => {
    this.setState({ editDialogOpen: false }, this.getAllUserDefenses);
  };

  render() {
    const { classes } = this.props;
    this.getAllUserDefenses.bind(this)

    // TODO: In componentDidMount- initialize the defenses that the user is sign up to.
    // How? - get Defense Registerst that has the user id in the student list,
    // And then get the details of the Defense and course (May use the quary from defense ...)
    // Fields I presented: CourseName, Date, Hour, lecturer.
    // also need to get register id and pass it so we'll know what register to change
    //Need to select min&max num of students, and array of students in the group to pass to the edit & delete

    const handleDeleteClickOpen = (
      regToDel,
      hour,
      minStudents,
      numOfstudents,
      members,
      courseName
    ) => {
      this.setState({ defenseId: regToDel });
      this.setState({ hour: hour });
      this.setState({ minStudents: minStudents });
      this.setState({ numOfstudents: numOfstudents });
      this.setState({ groupMembers: members });
      this.setState({ courseName: courseName });
      this.setState({ DeleteDialogOpen: true });
    };

    const handleDeleteClose = () => {
      this.setState({ DeleteDialogOpen: false });
      this.getAllUserDefenses()
    };

    return (
      <div className={classes.root}>
        <h1 className={classes.mainTitle}> ההגנות שלי</h1>
        <br />
        <Grid container spacing={2}>
          {this.state.myDefenses.map((elem) => (
            <Grid item key={this.state.myDefenses.indexOf(elem)}>
              <Card className={classes.card}>
                <CardContent>
                  <Typography className={classes.text} gutterBottom variant="h5" component="h2"> {elem.courseName} </Typography>
                  <Typography className={classes.text} variant="h6" color="textSecondary"> {elem.date}</Typography>
                  <Typography className={classes.text} variant="body2" color="textSecondary" component="p"> מרצה: {elem.lecturer} </Typography>
                  <Typography className={classes.text} variant="body2" color="textSecondary" component="p"> שעות: {elem.startTime} - {elem.endTime} </Typography>
                  <Typography className={classes.text} variant="body2" color="textSecondary" component="p"> קבוצה מס': {elem.hour} </Typography>
                </CardContent>
                <CardActions className={classes.cardButton}>
                  <Fab
                    className={classes.deleteBtn}
                    size="small"
                    aria-label="delete"
                    onClick={() =>
                      handleDeleteClickOpen(
                        elem.defenseId,
                        elem.hour,
                        elem.minStudents,
                        elem.students.length,
                        elem.students,
                        elem.courseName
                      )
                    }
                  >
                    <DeleteIcon />
                  </Fab>
                  <Fab
                    className={classes.editBtn}
                    size="small"
                    aria-label="edit"
                    onClick={() => {
                      this.setState({ currentRegister: elem.currentObj })
                      this.handleEditOpen()
                    }}
                  >
                    <EditIcon />
                  </Fab>
                </CardActions>
              </Card>
              <DeleteDefenseDialog
                open={this.state.DeleteDialogOpen}
                onClose={handleDeleteClose}
                defenseId={this.state.defenseId}
                hour={this.state.hour}
                userId={this.state.userId}
                minStudents={this.state.minStudents}
                studentsNum={this.state.numOfstudents}
                groupMembers={this.state.groupMembers}
                courseName={this.state.courseName}
              />
              <Dialog open={this.state.editDialogOpen} onClose={this.handleEditClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                <Edit
                  userId={this.state.userId}
                  register={this.state.currentRegister}
                  dialogClose={this.handleEditClose} />
              </Dialog>
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(MyDefenses);
