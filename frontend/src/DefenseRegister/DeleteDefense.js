import React from "react";
import axios from "axios";
import { styles } from "./DefenseRegister.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  Button,
  withStyles,
} from "@material-ui/core";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


class DeleteDefenseDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      defenseId: this.props.defenseId,
      hour: this.props.hour,
      userId: this.props.userId,
      minStudents: this.props.minStudents,
      studentsNum: this.props.studentsNum,
      groupMembers: this.props.groupMembers,
      courseName: this.props.courseName,
      minMaxErrorsSnackbar: false,
      errorsSnackbar: false,
      groupDeletedOpen: false,
      userDeletedOpen: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.defenseId !== this.props.defenseId || prevProps.hour !== this.props.hour) {
      this.setState({
        defenseId: this.props.defenseId,
        hour: this.props.hour,
        userId: this.props.userId,
        minStudents: this.props.minStudents,
        studentsNum: this.props.studentsNum,
        groupMembers: this.props.groupMembers,
        courseName: this.props.courseName,
        minMaxErrorsSnackbar: false,
        errorsSnackbar: false,
        groupDeletedOpen: false,
        userDeletedOpen: false,
      });
    }
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;

    const cancelAction = () => {
      this.setState({
        minMaxErrorsSnackbar: false,
        errorsSnackbar: false,
        groupDeletedOpen: false,
        userDeletedOpen: false,
      }, onClose)
    }

    const deleteAllStudents = () => {
      var data = {
        defenseId: this.state.defenseId,
        hour: this.state.hour,
        groupMembers: this.state.groupMembers,
        userId: this.state.userId,
        courseName: this.state.courseName,
        isEdit: false
      };
      axios
        .post(`/api/removeAllStudents`, data, {
          withCredentials: true,
        })
        .then((res) => {
          this.setState({ groupDeletedOpen: true })
        })
        .catch((err) => {
          this.setState({ errorsSnackbar: true })
        });
      this.setState({ defenseId: "", userId: "" });
    };

    const deleteCurrentStudent = () => {
      var filteredGroup = this.state.groupMembers.filter(
        (st) => st !== this.state.userId
      )
      var data = {
        defId: this.state.defenseId,
        hour: this.state.hour,
        groupMembers: filteredGroup,
      };

      if (this.state.studentsNum - 1 < this.state.minStudents) {
        this.setState({ minMaxErrorsSnackbar: true })
      } else {
        axios
          .post(`/api/deleteUserFromRegister`, data, {
            withCredentials: true,
          })
          .then((res) => {
            this.setState({ userDeletedOpen: true })
          })
          .catch((err) => {
            this.setState({ errorsSnackbar: true })
          });
      }
      this.setState({ defenseId: "", userId: "" });
    };

    return (
      <div className={classes.root}>
        <Dialog
          className={classes.dialog}
          open={isOpen}
          onClose={() => cancelAction()}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"מחיקת רישום להגנה"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              בחר באפשרות המחיקה הרצויה:
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={deleteCurrentStudent}
              className={classes.deleteBtn}
            >
              הסר אותי מהקבוצה
            </Button>
            <Button onClick={deleteAllStudents} className={classes.deleteBtn}>
              הסר את כל הקבוצה
            </Button>
            <Button onClick={() => cancelAction()} className={classes.cancelBtn}>
              ביטול
            </Button>
          </DialogActions>
          <Snackbar open={this.state.errorsSnackbar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() => this.setState({ errorsSnackbar: false })}
            >
              התקבלה שגיאה במחיקת הרישום
            </Alert>
          </Snackbar>
          <Snackbar
            open={this.state.groupDeletedOpen}
            autoHideDuration={6000}
          >
            <Alert
              severity="success"
              onClose={() =>
                this.setState({ groupDeletedOpen: false }, onClose)
              }
            >
              מחקת את כל הקבוצה מההגנה- הקפידו לבצע רישום מחודש
            </Alert>
          </Snackbar>
          <Snackbar
            open={this.state.userDeletedOpen}
            autoHideDuration={6000}
          >
            <Alert
              severity="success"
              onClose={() =>
                this.setState({ userDeletedOpen: false }, onClose)
              }
            >
              מחקת את עצמך להגנה- הקפד להירשם מחדש
            </Alert>
          </Snackbar>
          <Snackbar open={this.state.minMaxErrorsSnackbar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() => cancelAction()}
            >
              אין אפשרות לבצע מחיקה עצמית בשל חריגה ממינימום בקבוצה- מחק את הקבוצה או פנה למרצה
            </Alert>
          </Snackbar>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(DeleteDefenseDialog);
