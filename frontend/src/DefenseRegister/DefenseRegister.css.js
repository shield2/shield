export const styles = (theme) => ({
    root: {
        direction: "rtl",
        margin: "5%",
        marginRight: 210,
        flexGrow: 1,
    },
    mainTitle: {
        direction: "rtl",
        textAlign: "right",
        textShadow: "2px 2px #b5c5b9"
    },
    title: {
        direction: "rtl",
        textAlign: "right",
    },
    cancelBtn: {
        backgroundColor: "#9e9e9e",
        color: "white",
        margin: 5,
    },
    deleteBtn: {
        backgroundColor: "#e57373",
        color: "white",
        margin: 5,
    },
    text: {
        margin: "5%",
        textAlign: "right",
        direction: "rtl",
    },
    dialog: {
        direction: "rtl",
        textAlign: "right"
    },
    registerDiv: {
        direction: "rtl",
        margin: "5%",
        flexGrow: 1,
    },
    editDiv: {
        direction: "rtl",
        margin: "5%",
        flexGrow: 1,
    },
    btnSubmit: {
        marginRight: "45%",
        marginBottom: "3%"
    },
    btnDelete: {
        marginRight: "2%",
    },
    input: {
        margin: 5,
    },
    btnAdd: {
        backgroundColor: "#4d94ff",
        color: "white",
        margin: 5
    },
    paper: {
        marginTop: "1%"
    },
    courseName: {
        paddingRight: "2%",
        width: "70%"
    }
});