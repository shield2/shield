import React from 'react';
import { Form, Col } from "react-bootstrap";
import { styles } from './DefenseRegister.css.js';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import DialogActions from "@material-ui/core/DialogActions";
import { Fab, Button, Tooltip, IconButton, withStyles } from '@material-ui/core';
import axios from "axios";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Edit extends React.Component {

    constructor(props) {
        super();

        this.state = {
            userId: props.userId,
            obj: props.register,
            course: props.register.course_properties,
            students: props.register.groupMembers,
            originalNumber: props.register.groupMembers.length,
            hours: [],
            time: props.register.hour,
            originalTime: props.register.hour,
            // //props
            minStudents: props.register.course_properties.minStudents,
            maxStudents: props.register.course_properties.maxStudents,
            notInCourse: "",
            existStudent: "",
            snackMinimum: false,
            snackPickHour: false,
            snackDoubleId: false,
            snackSuccess: false,
            snackUsedHour: false,
            snackExistStudent: false,
            snackStudentNotInCourse: false,
            dialogClose: props.dialogClose
        };
    }

    componentDidMount() {

        const data = {
            defenseId: this.state.obj.defenseId,
            groupMembers: this.state.students
        };

        axios.post(`/api/getDefenseRegisterById`, data, { withCredentials: true }).then(res => {
            this.setState({ hours: res.data })
        }).catch(err => {
            // console.log("error")
        })
    }

    handleTimeChange = evt => {
        this.setState({ time: evt.target.value });
    };

    handleStudentIDChange = idx => evt => {
        const newStudents = this.state.students.map((student, sidx) => {
            if (idx !== sidx) return student;
            return evt.target.value;
        });

        this.setState({ students: newStudents });
    };

    handleSubmit = async evt => {
        const { time, students } = this.state;

        if (time === "0") {
            this.setState({ snackPickHour: true })
        }
        else if (students.length < this.state.minStudents) {
            this.setState({ snackMinimum: true })
        }
        else {
            var data = {
                defenseId: this.state.obj.defenseId,
                hour: this.state.time,
                students: students
            }

            var studentsInCourse;
            await axios.get(`/api/getCourseByID?id=${this.state.course._id}`).then(res => {
                studentsInCourse = res.data.students
            })
            axios.post(`/api/getDefenseRegistersStudents`, { hour: this.state.originalTime, defenseId: data.defenseId }, { withCredentials: true }).then(res => {
                var members = [];
                res.data.forEach(element => { members = [...members, element.groupMembers] });

                members = members.flat()

                var alreadyExist = false
                var allStudentsInCourse = true
                students.forEach(student => {
                    if (members.filter(e => e === student).length > 0) {
                        alreadyExist = true
                        this.setState({ existStudent: student })
                    }
                    if (studentsInCourse.filter(e => e.toString() === student).length <= 0) {
                        allStudentsInCourse = false
                        this.setState({ notInCourse: student })
                    }
                })

                if (!alreadyExist) {
                    if (allStudentsInCourse) {
                        if (new Set(students).size !== students.length) {
                            this.setState({ snackDoubleId: true })
                        } else {
                            if (data.hour !== this.state.originalTime) {
                                axios.post(`/api/register`, data, { withCredentials: true }).then(res => {
                                    if (res.data.n === 1) {
                                        var data2 = {
                                            defenseId: data.defenseId,
                                            hour: this.state.originalTime,
                                            groupMembers: data.students,
                                            userId: this.state.userId,
                                            courseName: this.state.course.name,
                                            isEdit: true
                                        };
                                        axios.post(`/api/removeAllStudents`, data2, { withCredentials: true }).then(res => {
                                            this.setState({ snackSuccess: true })
                                            this.state.dialogClose()
                                        })
                                    } else {
                                        this.setState({ snackUsedHour: true })
                                    }
                                }).catch(err => {
                                    // console.log("error")
                                })
                            } else {
                                var newGroup = { defId: data.defenseId, hour: data.hour, groupMembers: data.students }
                                axios.post(`/api/deleteUserFromRegister`, newGroup, { withCredentials: true, }).then((res) => {
                                    this.setState({ snackSuccess: true })
                                    this.state.dialogClose()
                                }).catch((err) => {
                                    // console.log(err);
                                })
                            }
                        }
                    } else {
                        this.setState({ snackStudentNotInCourse: true })
                    }
                } else {
                    this.setState({ snackExistStudent: true })
                }
            }).catch(err => {
                // console.log(err)
            })
        }
    };

    handleAddStudent = () => {
        this.setState({
            students: this.state.students.concat("")
        })
    };

    handleRemoveStudent = idx => () => {
        this.setState({
            students: this.state.students.filter((s, sidx) => idx !== sidx)
        });
    };

    handleClose = (event, reason) => {
        this.setState({
            snackMinimum: false,
            snackPickHour: false,
            snackDoubleId: false,
            snackExistStudent: false,
            snackStudentNotInCourse: false,
            snackSuccess: false,
            snackUsedHour: false
        });
    };
    render() {
        const { classes } = this.props
        const { hours, obj, course } = this.state

        return (
            <div className={classes.editDiv}>
                <div>
                    <h5 className={classes.title}>עדכון מועד הגנה</h5>
                    <h6 className={classes.dialog}>{course.name + '- ' + course.lecturer} </h6>
                    <h6 className={classes.dialog}>{'רשום להגנה בתאריך ' + obj.date}</h6>
                    <h6 className={classes.dialog}>{' קבוצה מס\' ' + obj.hour}</h6>
                </div>
                <Form className={classes.text}>
                    <Form.Row>
                        <Form.Label column >מס' קבוצה:</Form.Label>
                        <Col className={classes.input}>
                            <Form.Control as="select"
                                value={this.state.time} onChange={this.handleTimeChange} className={classes.list}>
                                <option item key="0">0</option>
                                {hours.map(elem => (
                                    <option item key={hours.indexOf(elem)}>{elem.hour}</option>
                                ))}
                            </Form.Control>
                        </Col>
                    </Form.Row>
                    <br />

                    <h6>תעודות זהות סטודנטים</h6>
                    {this.state.students.map((student, idx) => (
                        <div className="student">
                            <input
                                placeholder={`תעודת זהות סטודנט ${idx + 1}`}
                                value={student}
                                disabled={((idx + 1) <= this.state.originalNumber) ? true : false}
                                onChange={this.handleStudentIDChange(idx)}
                                className={classes.input}
                            />
                            {idx >= (this.state.originalNumber) &&
                                <Tooltip title="מחק סטודנט" aria-label="delete">
                                    <IconButton aria-label="delete" onClick={this.handleRemoveStudent(idx)} className={classes.btnDelete}>
                                        <DeleteIcon fontSize="small" />
                                    </IconButton>
                                </Tooltip>}
                        </div>
                    ))}

                    {this.state.students.length < (this.state.maxStudents) &&
                        <Tooltip title="הוסף סטודנט" aria-label="add">
                            <Fab
                                className={classes.btnAdd}
                                aria-label="add"
                                type="button"
                                onClick={this.handleAddStudent}
                                size="small">
                                <AddIcon />
                            </Fab>
                        </Tooltip>
                    }
                    <br /><br />
                    <DialogActions>
                        <Button onClick={this.state.dialogClose} color="primary">ביטול</Button>
                        <Button onClick={this.handleSubmit} color="primary">עדכן</Button>
                    </DialogActions>
                </Form>
                <Snackbar open={this.state.snackPickHour} autoHideDuration={6000} onClose={this.handleClose}>
                    <Alert onClose={this.handleClose} severity="error">
                        חובה לבחור מס' קבוצה
                    </Alert>
                </Snackbar>
                <Snackbar open={this.state.snackMinimum} autoHideDuration={6000} onClose={this.handleClose}>
                    <Alert onClose={this.handleClose} severity="error">
                        מינימום {this.state.minStudents} סטודנטים בפרויקט
                    </Alert>
                </Snackbar>
                <Snackbar open={this.state.snackDoubleId} autoHideDuration={6000} onClose={this.handleClose}>
                    <Alert onClose={this.handleClose} severity="error">
                        ניסיון לרשום כפול של ת.ז. נכשל, אין צורך לרשום את עצמך
                    </Alert>
                </Snackbar>
                <Snackbar open={this.state.snackSuccess} autoHideDuration={6000} onClose={this.handleClose}>
                    <Alert onClose={this.handleClose} severity="success">
                        העריכה להגנה בוצע בהצלחה
                    </Alert>
                </Snackbar>
                <Snackbar open={this.state.snackUsedHour} autoHideDuration={6000} onClose={this.handleClose}>
                    <Alert onClose={this.handleClose} severity="error">
                        השעה שנבחרה נתפסה ע״י קבוצה אחרת, יש לבחור שעת הגנה אחרת
                    </Alert>
                </Snackbar>
                <Snackbar open={this.state.snackStudentNotInCourse} autoHideDuration={6000} onClose={this.handleClose}>
                    <Alert onClose={this.handleClose} severity="info">
                        הסטודנט {this.state.notInCourse} לא קיים ברשימת הסטודנטים של הקורס
                    </Alert>
                </Snackbar>
                <Snackbar open={this.state.snackExistStudent} autoHideDuration={6000} onClose={this.handleClose}>
                    <Alert onClose={this.handleClose} severity="info">
                        הסטודנט {this.state.existStudent} רשום להגנה זו במועד/בשעה אחרת. אין אפשרות להירשם פעמיים לאותה הגנה
                    </Alert>
                </Snackbar>
            </div>
        );
    }
}

export default withStyles(styles)(Edit);
