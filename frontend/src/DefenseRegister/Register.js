import React from 'react';
import { Form, Col } from "react-bootstrap";
import { styles } from './DefenseRegister.css.js';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import DialogActions from "@material-ui/core/DialogActions";
import { Fab, Button, Tooltip, IconButton, withStyles } from '@material-ui/core';
import axios from "axios";
import { navigate } from '@reach/router';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Register extends React.Component {

  constructor(props) {
    super();

    this.state = {
      closeDialog: props.closeDialog,
      defenseId: props.defense._id,
      time: "",
      userId: props.userId,
      defense: props.defense,
      courseId: props.defense.courseId,
      isAdmin: false,
      // defense: [],
      students: [],
      hours: [],
      //props
      minStudents: props.defense.minStudents,
      maxStudents: props.defense.maxStudents,
      notInCourse: "",
      existStudent: "",
      snackMinimum: false,
      snackPickHour: false,
      snackDoubleId: false,
      snackSuccess: false,
      snackUsedHour: false,
      snackExistStudent: false,
      snackStudentNotInCourse: false
    };
  }

  async componentDidMount() {
    const students = [];
    for (let index = 0; index < (this.state.minStudents - 1); index++) {
      students.push({ id: null })
    }

    const data = {
      defenseId: this.state.defenseId
    };

    await axios.get(`/api/getUser`).then((response) => {
      this.setState({ isAdmin: response.data.isAdmin });
    })

    axios.post(`/api/getDefenseRegisterById`, data, { withCredentials: true }).then(res => {
      this.setState({ hours: res.data })
    }).catch(err => {
      // console.log("error")
    })

    this.setState({
      students: students
    });
  }

  handleTimeChange = evt => {
    this.setState({ time: evt.target.value });
  };

  handleStudentIDChange = idx => evt => {
    const newStudents = this.state.students.map((student, sidx) => {
      if (idx !== sidx) return student;
      return { ...student, id: evt.target.value };
    });

    this.setState({ students: newStudents });
  };

  handleSubmit = async evt => {
    var newArrStudents = this.state.students.filter(a => (a.id != null && a.id !== ""))
    await this.setState({ students: newArrStudents })

    const { time, students } = this.state;
    const isAdmin = this.state.isAdmin

    if (time === "") {
      this.setState({ snackPickHour: true })
    } else if ((!isAdmin && students.length < this.state.minStudents - 1) ||
      (isAdmin && students.length < 1)) {
      this.setState({ snackMinimum: true })
    } else {
      var sid = []
      students.forEach(element => { sid = [...sid, element.id] });

      if (!isAdmin)
        sid = sid.concat(this.state.userId)

      var data = {
        defenseId: this.state.defenseId,
        hour: this.state.time,
        students: sid
      }

      var studentsInCourse;
      await axios.get(`/api/getCourseByID?id=${this.state.courseId}`).then(res => {
        studentsInCourse = res.data.students
      })
      axios.post(`/api/getDefenseRegistersStudents`, { hour: this.state.time, defenseId: this.state.defenseId }, { withCredentials: true }).then(res => {
        var members = [];
        res.data.forEach(element => { members = [...members, element.groupMembers] });

        members = members.flat()

        var alreadyExist = false
        var allStudentsInCourse = true
        sid.forEach(student => {
          if (members.filter(e => e === student).length > 0) {
            alreadyExist = true
            this.setState({ existStudent: student })
          }
          if (studentsInCourse.filter(e => e.toString() === student).length <= 0) {
            allStudentsInCourse = false
            this.setState({ notInCourse: student })
          }
        })

        if (!alreadyExist) {
          if (allStudentsInCourse) {
            if (new Set(sid).size !== sid.length) {
              this.setState({ snackDoubleId: true })
            } else {
              axios.post(`/api/register`, data, { withCredentials: true }).then(res => {
                if (res.data.n === 1) {
                  this.setState({ snackSuccess: true })
                  navigate('/')
                } else {
                  this.setState({ snackUsedHour: true })
                }
              }).catch(err => {
                // console.log("error")
              })
            }
          } else {
            this.setState({ snackStudentNotInCourse: true })
          }
        } else {
          this.setState({ snackExistStudent: true })
        }
      }).catch(err => {
        // console.log(err)
      })

    }
  };

  handleAddStudent = () => {
    this.setState({
      students: this.state.students.concat([{ id: "" }])
    })
  };

  handleRemoveStudent = idx => () => {
    this.setState({
      students: this.state.students.filter((s, sidx) => idx !== sidx)
    });
  };

  handleClose = (event, reason) => {
    this.setState({
      snackMinimum: false,
      snackPickHour: false,
      snackDoubleId: false,
      snackExistStudent: false,
      snackStudentNotInCourse: false,
      snackSuccess: false,
      snackUsedHour: false
    });
  };
  handleCloseSuccess = () => {
    this.setState({
      snackSuccess: false,
    })
    this.state.closeDialog()
  }
  render() {
    const { classes } = this.props
    const { hours, defense } = this.state
    return (
      <div className={classes.registerDiv}>
        <div>
          <h5 className={classes.title}> הרשמה למועד הגנה</h5>
        </div>
        <Form className={classes.text}>
          <Form.Row>
            {/* <Form.Label > */}

            {/* </Form.Label> */}
            {/* <Col className={classes.courseName}> */}
            שם הקורס: <div className={classes.courseName}> {defense.name} </div>
            {/* </Col> */}
          </Form.Row>

          <Form.Row>
            <Form.Label column >
              מרצה:
                    </Form.Label>
            <Col >
              <Form.Control plaintext readOnly defaultValue={defense.lecturer} />
            </Col>
          </Form.Row>

          <Form.Row>
            <Form.Label column >
              תאריך ההגנה:
                    </Form.Label>
            <Col >
              <Form.Control plaintext readOnly defaultValue={defense.date} />
            </Col>
          </Form.Row>

          <Form.Row>
            <Form.Label column >
              שעות:
                    </Form.Label>
            <Col >
              <Form.Control plaintext readOnly defaultValue={`${defense.startTime} - ${defense.endTime}`} />
            </Col>
          </Form.Row>

          <Form.Row>
            <Form.Label column >
              זמן להגנה:
                    </Form.Label>
            <Col >
              <Form.Control plaintext readOnly defaultValue={`${defense.time} דקות`} />
            </Col>
          </Form.Row>

          <Form.Row>
            <Form.Label column >מס' קבוצה:</Form.Label>
            <Col >
              <Form.Control as="select"
                value={this.state.time} onChange={this.handleTimeChange} className={classes.list}>
                <option item key="0">0</option>
                {hours.map(elem => (
                  <option item key={hours.indexOf(elem)}>{elem.hour}</option>
                ))}
              </Form.Control>
            </Col>
          </Form.Row>
          <br />

          <h6>תעודות זהות סטודנטים</h6>
          {this.state.students.map((student, idx) => (
            <div className="student">
              <input
                placeholder={`תעודת זהות סטודנט ${idx + 1}`}
                value={student.id}
                onChange={this.handleStudentIDChange(idx)}
                className={classes.input}
              />
              {(this.state.isAdmin || (idx >= (this.state.minStudents - 1))) &&
                <Tooltip title="מחק סטודנט" aria-label="delete">
                  <IconButton aria-label="delete" onClick={this.handleRemoveStudent(idx)} className={classes.btnDelete}>
                    <DeleteIcon fontSize="small" />
                  </IconButton>
                </Tooltip>}
            </div>
          ))}

          {(this.state.isAdmin || (this.state.students.length < (this.state.maxStudents - 1))) &&
            <Tooltip title="הוסף סטודנט" aria-label="add">
              <Fab
                className={classes.btnAdd}
                aria-label="add"
                type="button"
                onClick={this.handleAddStudent}
                size="small">
                <AddIcon />
              </Fab>
            </Tooltip>
          }
          <br /><br />
          <DialogActions>
            <Button onClick={this.state.closeDialog} color="primary">
              ביטול
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              הרשם
            </Button>
          </DialogActions>
        </Form>
        <Snackbar open={this.state.snackPickHour} autoHideDuration={6000} onClose={this.handleClose}>
          <Alert onClose={this.handleClose} severity="error">
            חובה לבחור מספר קבוצה
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackMinimum} autoHideDuration={6000} onClose={this.handleClose}>
          <Alert onClose={this.handleClose} severity="error">
            מינימום {this.state.minStudents} סטודנטים בפרויקט
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackDoubleId} autoHideDuration={6000} onClose={this.handleClose}>
          <Alert onClose={this.handleClose} severity="error">
            ניסיון לרשום כפול של ת.ז. נכשל, אין צורך לרשום את עצמך
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackUsedHour} autoHideDuration={6000} onClose={this.handleClose}>
          <Alert onClose={this.handleClose} severity="error">
            השעה שנבחרה נתפסה ע״י קבוצה אחרת, יש לבחור שעת הגנה אחרת
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackStudentNotInCourse} autoHideDuration={6000} onClose={this.handleClose}>
          <Alert onClose={this.handleClose} severity="info">
            הסטודנט {this.state.notInCourse} לא קיים ברשימת הסטודנטים של הקורס
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackExistStudent} autoHideDuration={6000} onClose={this.handleClose}>
          <Alert onClose={this.handleClose} severity="info">
            הסטודנט {this.state.existStudent} רשום להגנה זו במועד/בשעה אחרת. אין אפשרות להירשם פעמיים לאותה הגנה
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackSuccess} autoHideDuration={6000} onClose={this.handleCloseSuccess}>
          <Alert onClose={this.handleCloseSuccess} severity="success">
            הרישום להגנה בוצע בהצלחה
          </Alert>
        </Snackbar>
      </div>
    );
  }
}

export default withStyles(styles)(Register);
