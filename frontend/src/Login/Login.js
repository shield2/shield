import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useStyles } from './Login.css.js';
import { TextField, CardActions, Card, CardContent } from '@material-ui/core'
import { navigate } from '@reach/router'
import axios from 'axios';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

export default function Login(props) {
  const classes = useStyles();

  const [snackLogin, setSnackLogin] = useState(false);
  const [username, setUsername] = useState("");
  const [id, setId] = useState("");

  function validateForm() {
    return username.length > 0 && id.length === 9;
  }

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  function handleSubmit(event) {
    const user = {
      id,
      username,
    };
    axios.post(`/api/login`, user, { withCredentials: true }).then(res => {
      document.cookie = "userAdded=0"
      res.data.isAdmin ? navigate('/AdminHome') : navigate('/')
    }).catch(err => {
      setSnackLogin(true)
    })
  }

  function handleClose(event, reason) {
    setSnackLogin(false)
  }

  return (
    <div>
      <Form className={classes.loginForm}>
        <Card className={classes.card}>
          <CardContent>
            <Form.Row>
              <TextField className={classes.text}
                label="שם פרטי"
                placeholder="הכנס שם פרטי"
                value={username}
                onChange={e => setUsername(e.target.value)}
                type="search"
                variant="outlined" />
            </Form.Row>
            <br></br>
            <Form.Row>
              <TextField className={classes.text}
                label="תעודת זהות"
                placeholder="הכנס תעודת זהות"
                value={id}
                onChange={e => setId(e.target.value)}
                type="search"
                variant="outlined" />
            </Form.Row>
          </CardContent>
          <CardActions className={classes.btnSubmit} >
            <br></br>
            <Button variant="primary" disabled={!validateForm()} onClick={() => { handleSubmit() }}>
              התחבר
            </Button>
          </CardActions>
        </Card>
      </Form>
      <Snackbar open={snackLogin} autoHideDuration={6000} onClose={() => handleClose()}>
        <Alert onClose={() => handleClose()} severity="error">
          שגיאה בפרטי התחברות אנא נסה שנית
        </Alert>
      </Snackbar>
    </div>
  );
}