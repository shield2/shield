import React from "react";
import { styles } from "./Home.css.js";
import {
  withStyles,
  Card,
  CardContent,
  Typography,
  CardActions,
  Grid,
  Fab,
  Tooltip,
} from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
import AssignmentOutlinedIcon from "@material-ui/icons/AssignmentOutlined";
import axios from "axios";
import Register from "../DefenseRegister/Register.js";

class Home extends React.Component {
  constructor(props) {
    super(props);
    // console.log(this.props.id)
    // console.log(props)
    this.state = {
      allDefenses: [],
      userCourses: [],
      userId: this.props.id,
      name: this.props.name,
      isAdmin: this.props.isAdmin,
      open: false,
      currentDefense: '',
      bla: this.props.bla
    };
  }

  componentDidMount() {
    this.getUserDefenses()
  }

  componentDidUpdate(prevProps) {
    if (this.state.userId === "") {
      this.setState({ userId: this.props.id }, this.getUserDefenses)
    }
  }

  getUserDefenses() {
    var data = { uId: this.state.userId }
    axios
      .post(`/api/userDefenses`, data, { withCredentials: true })
      .then((res) => {
        // console.log(res.data)
        this.setState({ allDefenses: res.data });
      })
      .catch((err) => {
        // console.log("get courses error");
      });

    // console.log(data)
    axios.post(`/api/getCoursesOfStudent`, data, { withCredentials: true })
      .then((res) => {
        this.setState({ userCourses: res.data });
      })
      .catch((err) => {
        // console.log("get courses error");
      });
  }



  //   onCourseChangeHandler = (e) => {
  //     this.setState({
  //       course: e.target.value
  //     })
  //   }

  //   onLecturerChangeHandler = (e) => {
  //     this.setState({
  //       lecturer: e.target.value
  //     })
  //   }

  //   onSemeaterChangeHandler = (e) => {
  //     this.setState({
  //       semester: e.target.value
  //     })
  //   }

  //   handleSearch = () => {
  //     var data = {
  //       course: this.state.course,
  //       year: this.state.lecturer,
  //       semester: this.state.semester
  //     };
  //     axios.post(`/api/searchDefenses`, data, { withCredentials: true }).then(res => {
  //       const defenses = res.data;
  //       console.log(defenses)
  //       if(defenses)
  //       {
  //         this.setState({allDefenses: defenses})
  //       }

  //     }).catch(err => {
  //       console.log("error")
  //     })
  //   }

  handleClickOpen = () => {
    this.setState({ open: true })
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  formatDate = (date) => {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }
    return new Date(`${mm}/${dd}/${yyyy}`);
  }

  formatElemDate = (date) => {
    const dates = date.split('/');
    var dd = dates[0];
    var mm = dates[1];
    var yyyy = dates[2];

    return new Date(`${mm}/${dd}/${yyyy}`);
  }

  render() {
    const { classes } = this.props;
    var today = this.formatDate(new Date());
    return (
      <div>
        <div className={classes.root}>
          {this.state.isAdmin ?
            <h1 className={classes.mainTitle}>רישום חריג להגנה</h1>
            : <h1 className={classes.mainTitle}>הגנות</h1>}
          <br />
          <Grid container spacing={2}>
            {this.state.allDefenses
              // .filter((defense) => {
              //   return (
              //     defense.courseName.includes(this.state.course) &&
              //     defense.lecturer.includes(this.state.lecturer) &&
              //     defense.semester.includes(this.state.semester)
              //   );
              // })
              .map((elem, index) => (
                (today <= this.formatElemDate(elem.date)) ?
                  (<Grid item key={index}>
                    <Card className={classes.card}>
                      <CardContent className={classes.cardContent}>
                        <Typography className={classes.text} variant="h5" component="h2" > {elem.name} </Typography>
                        <Typography className={classes.text} variant="h6" color="textSecondary" > {elem.date} </Typography>
                        <Typography className={classes.text} variant="h6" color="textSecondary" > {} </Typography>
                        <Typography className={classes.text} variant="body2" color="textSecondary" component="p"> מרצה: {elem.lecturer} </Typography>
                        <Typography className={classes.text} variant="body2" color="textSecondary" component="p"> שעות: {elem.startTime} - {elem.endTime} </Typography>
                      </CardContent>
                      <CardActions className={classes.cardAction}>
                        <Tooltip title="הרשמה" aria-label="add">
                          <Fab className={classes.registerBtn} size="small" aria-label="register"
                            onClick={() => {
                              this.setState({ currentDefense: elem })
                              this.handleClickOpen()
                            }}>
                            <AssignmentOutlinedIcon />
                          </Fab>
                        </Tooltip>
                      </CardActions>
                    </Card>
                  </Grid>) : null
              ))}
          </Grid>
        </div>
        <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
          <Register closeDialog={this.handleClose} defense={this.state.currentDefense} userId={this.state.userId} />
        </Dialog>
      </div>
    );
  }
}
export default withStyles(styles)(Home);
