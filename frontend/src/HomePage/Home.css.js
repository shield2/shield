export const styles = (theme) => ({
  root: {
    direction: "rtl",
    margin: "5%",
    marginRight: 210,
    flexGrow: 1,
  },

  mainTitle: {
    direction: "rtl",
    textAlign: "right",
    textShadow: "2px 2px #b5c5b9",
    marginBottom: "30px"
  },

  card: {
    direction: "rtl",
    textAlign: "right",
    width: 230,
    height: 170,
    backgroundColor: "#f5f5f5",
    margin: 10
  },

  registerBtn: {
    backgroundColor: "#4d94ff",
    color: "white",
  },

  cardContent: {
    padding: "5px 16px"
  },

  cardAction: {
    float: "left"
  },
})