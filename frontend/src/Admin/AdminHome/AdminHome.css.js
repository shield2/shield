export const styles = () => ({
  root: {
      direction: "rtl",
      margin: "5%",
      marginRight: 210,
      flexGrow: 1,
      textAlign: "right"
  },

  list: {
    width: '100%',
    maxWidth: 360,
  },

  nested: {
    paddingLeft: 4,
  },

  image: {
      marginTop: "5px",
  },

  mainTitle: {
      direction: "rtl",
      textAlign: "right",
      textShadow: "2px 2px #b5c5b9",
      marginBottom: "30px"
  },

note: {
    color: "#b71c1c",
    fontSize: "12px",
    marginTop: "10px",
},

  editBtn: {
      backgroundColor: "#00bfa5",
      color: "white",
      margin: 5,
  },
  
  CancelBtn: {
    backgroundColor: "#9e9e9e",
    color: "white",
    margin: 5,
},

  deleteBtn: {
      backgroundColor: "#d8626d",
      color: "white",
      margin: 5,
  },
  card: {
      width: 300,
      height: 260,
      backgroundColor: "#f5f5f5",
      margin: 10,
  },

  defenseCard: {
      width: 240,
      height: 220,
      backgroundColor: "#f5f5f5",
      margin: 10,
  },

  Papertext: {
      textAlign: "right",
      direction: "rtl",
      margin: '5%'
  },

  text: {
      textAlign: "right",
      direction: "rtl",
      marginRight: "15%"
  },

  search: {
      textAlign: "right",
      direction: "rtl",
      margin: 15
  },

  iconButton: {
      padding: 10
  },

  cardButton: {
      direction: "ltr",
  },

  //#dce775
  btnList: {
      backgroundColor: "#607d8b",
      color: "white",
      marginLeft: 1200,
      marginTop: 20,
      marginBottom: 20
  },

  btAddNewDefense: {
      backgroundColor: "#b0bec5",
      color: "white",
      marginLeft: 1200,
      marginTop: 20,
      marginBottom: 20
  },

  btnAdd: {
      backgroundColor: "#4d94ff",
      color: "white",
      marginLeft: 1200,
      marginTop: 20,
      marginBottom: 20
  },

  locBtn: {
      backgroundColor: "#f8f9fa",
      color: "black",
  },

  btnSubmit: {
      marginRight: "45%",
      marginBottom: "3%"
  },

  paper: {
      maxHeight: 1400,
      maxWidth: 750,
      marginTop: "1%",
      marginRight: "15%"
  },

  dialog: {
    direction: "rtl",
    textAlign: "right",
    position: "relative",
    width: "60% !important",
    height: "80% !important",
    top: "10% !important",
    left: "20% !important",
    maxHeight:'none',
},

dialogLable: {
    direction: "rtl",
    textAlign: "right",
    width: "100px !important",
    paddingLeft: "10px",
    display: "inline-block",
    height: "100%",
    verticalAlign: "middle",
    margin: "8px 0 4px"
},

  menu: {
      direction: 'rtl',
      marginLeft: "-13%",
  },

  thead: {
      fontWeight: "1000 !important"
  },
  
  input: {
    display: 'none',
  },

  addStudentBtn: {
    // marginRight: "55%",
    // marginTop: "-20%",
    marginRight: 193,
    //marginBottom: 20
  },

  btn: {
      marginTop:-60,
      marginRight: 470
  },
   
  popover: {
    pointerEvents: 'none',
  },
 

})

