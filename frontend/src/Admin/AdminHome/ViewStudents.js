import React from "react";
import axios from "axios";
import { styles } from "../AdminHome/AdminHome.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import {
  Button,
  withStyles,
} from "@material-ui/core";

class ViewStudentsDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      students: []
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.students !== this.props.students) {
      this.setState({ students: [] })
      this.CheckRegister()
    }
  }

  CheckRegister = async () => {
    await this.didRegister()
  }

  // didRegister = studentId => {
  //     var register = "לא רשום";
  //     axios.post(`/api/getMyDefenses`, { userId: studentId.toString() },{ withCredentials: true }).then((res) => {
  //             var defenses = res.data;
  //             if (!defenses.err) {
  //                 console.log("הגנות של סטודנט " + studentId)
  //                 console.log(defenses)
  //                 console.log(this.props.id)

  //                 if(defenses.find(defense => (defense.courseId === this.props.id.toString())))
  //                 {
  //                     register = "רשום";
  //                 }
  //             }
  //     });

  //     return register;
  // }

  didRegister = async () => {
    this.props.students.map(student => {
      axios.post(`/api/getMyDefenses`, { userId: student.toString() }, { withCredentials: true }).then((res) => {
        var defenses = res.data;
        var register = "לא רשום";

        if (!defenses.err) {
          // console.log("הגנות של סטודנט " + student)
          // console.log(defenses)

          if (defenses.find(defense => (defense.courseId === this.props.id.toString()))) {
            register = "רשום";
          }
        }

        var currStudent = {
          id: student,
          didRegister: register,
        };

        this.setState({ students: [...this.state.students, currStudent] })
      });
      return null
    })
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;

    return (
      <div className={classes.root}>
        <Dialog
          open={isOpen}
          onClose={onClose}
          aria-labelledby="form-dialog-title"
          className={classes.dialog}
        >
          <DialogTitle id="form-dialog-title">סטודנטים בקורס</DialogTitle>
          <DialogContent>
            <h6>
              מספר הסטודנטים בקורס: {this.props.students.length}
            </h6>
            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <TableRow background="#eeeeee">
                    <TableCell align="right">סטודנטים</TableCell>
                    <TableCell align="right">רישום להגנה בקורס</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.students.map(student => (
                    <TableRow key={student.id}>
                      <TableCell align="right">{student.id}</TableCell>
                      <TableCell align="right">{student.didRegister}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose} color="primary">
              סגור
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(ViewStudentsDialog);
