import React from "react";
import axios from "axios";
import { styles } from "./AdminHome.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Form, Col } from "react-bootstrap";
import { Button, TextField, withStyles } from "@material-ui/core";
import { ExcelRenderer } from "react-excel-renderer";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class AddCourseDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      courseName: "",
      lecturer: this.props.uname,
      lecturerID: this.props.uid,
      year: "",
      semester: "",
      minStudents: "",
      maxStudents: "",
      about: "",
      rows: [],
      cols: [],
      FileName: "",
      dataToSend: [],
      courseAddedSnackbar: false,
      errorsSnackbar: false,
      errList: "",
    };
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;

    const handleChanged = (e) => {
      this.setState({ [e.target.name]: e.target.value });
    };

    const cancelAction = () => {
      this.setState({
        courseName: "",
        lecturer: this.props.uname,
        lecturerID: this.props.uid,
        year: "",
        semester: "",
        minStudents: "",
        maxStudents: "",
        about: "",
        rows: [],
        cols: [],
        FileName: "",
        dataToSend: [],
        courseAddedSnackbar: false,
        errorsSnackbar: false,
        errList: "",
      }, onClose)
    }
    const checkFileFormat = () => {
      if (this.state.FileName !== "") {
        var fileTitles = this.state.rows[0];
        return (
          fileTitles[0] === "שם פרטי" &&
          fileTitles[1] === "שם משפחה" &&
          fileTitles[3] === "תעודת זהות" &&
          fileTitles[2] === "מייל" &&
          fileTitles.length === 4
        );
      }

      return 1;
    };

    const parseRowsToUsersArray = () => {
      var stList = [];
      var stRows = this.state.rows.slice(1);

      stRows.forEach((st) => {
        var currStudent = {
          firstName: st[0],
          lastName: st[1],
          id: st[3],
          mail: st[2],
          isAdmin: false,
        };
        stList.push(currStudent);
      });

      return stList;
    };

    const addCourse = () => {
      var data = {
        courseName: this.state.courseName,
        lecturer: this.state.lecturer,
        lecturerID: this.state.lecturerID,
        year: this.state.year,
        semester: this.state.semester,
        minStudents: this.state.minStudents,
        maxStudents: this.state.maxStudents,
        about: this.state.about,
      };

      var FileNameSplits = this.state.FileName.split(".");
      var filePrefix = FileNameSplits[FileNameSplits.length - 1];
      var text = /^[0-9]+$/;

      var errors = [];
      if (data.courseName === "") errors.push("לא הוזן שם הקורס");
      if (data.year === "") errors.push("לא הוזנה השנה בה מתקיים הקורס");
      if ((data.year !== "") && ((data.year.length !== 4) || (!text.test(data.year)))) errors.push("יש להזין שנה בפורמט YYYY מספרים בלבד");
      if (data.semester === "") errors.push("לא הוזן הסמסטר בו מתקיים הקורס");
      if (data.minStudents === "")
        errors.push("לא הוזן מספר מינימאלי של סטודנטים בקבוצה");
      if (data.maxStudents === "")
        errors.push("לא הוזן מספר מקסימלי של סטודנטים בקבוצה");
      if (data.maxStudents < data.minStudents)
        errors.push("מספר סטודנטים מקסימלי קטן מהמספר המינימאלי");
      if (filePrefix !== "" && filePrefix !== "xlsx" && filePrefix !== "xls")
        errors.push("קובץ הסטודנטים שנטען אינו בפורמט xls/xlsx כנדרש");
      if (!checkFileFormat())
        errors.push("עמודת המידע בקובץ הסטודנטים אינו בפורמט הנדרש");

      if (errors.length === 0) {
        var stList = "";

        if (this.state.FileName !== "") {
          stList = parseRowsToUsersArray();
        }

        data = { ...data, studentsList: stList };

        axios
          .post(`/api/addCourse`, data)
          .then((res) => {
            this.setState({ courseAddedSnackbar: true });

            this.setState({
              courseName: "",
              year: "",
              semester: "",
              minStudents: "",
              maxStudents: "",
              about: "",
              FileName: "",
            });
          })
          .catch((err) => {
            if (err.response.status === 400)
              this.setState({ errList: "שגיאה", errorsSnackbar: true });
            // console.log("Something went wrong");
          });
      } else {
        this.setState({ errList: errors.join(", "), errorsSnackbar: true });
        //alert(errors.join("\n"));
      }
    };

    const changeHandler = (event) => {
      let fileObj = event.target.files[0];
      this.setState({ [event.target.name]: event.target.value });

      if (fileObj) {
        //just pass the fileObj as parameter
        ExcelRenderer(fileObj, (err, resp) => {
          if (err) {
            // console.log(err);
          } else {
            // console.log(resp);
            this.setState({
              cols: resp.cols,
              rows: resp.rows,
            });
            // console.log(this.state);
          }
        });
      }
    };

    return (
      <div className={classes.root}>
        <Dialog
          open={isOpen}
          onClose={() => cancelAction()}
          aria-labelledby="form-dialog-title"
          className={classes.dialog}
          maxWidth="xl"
        >
          <DialogTitle id="form-dialog-title">הוספת קורס חדש</DialogTitle>
          <DialogContent>
            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                שם הקורס:
              </Form.Label>
              <TextField
                required
                autoFocus
                margin="dense"
                type="text"
                name="courseName"
                value={this.state.courseName}
                onChange={handleChanged}
              />
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                שנה:
              </Form.Label>
              <TextField
                margin="dense"
                type="text"
                name="year"
                value={this.state.year}
                onChange={handleChanged}
              />
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                סמסטר:
              </Form.Label>
              <Col sm="5">
                <Form.Control
                  as="select"
                  onChange={handleChanged}
                  name="semester"
                >
                  <option> </option>
                  <option>א</option>
                  <option>ב</option>
                  <option>קיץ</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                מינימום סטודנטים בקבוצה:
              </Form.Label>
              <Col sm="5">
                <Form.Control
                  as="select"
                  onChange={handleChanged}
                  name="minStudents"
                >
                  <option> </option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                מקסימום סטודנטים בקבוצה:
              </Form.Label>
              <Col sm="5">
                <Form.Control
                  as="select"
                  onChange={handleChanged}
                  name="maxStudents"
                >
                  <option> </option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                  <option>7</option>
                  <option>8</option>
                </Form.Control>
              </Col>
            </Form.Row>
            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                תיאור:
              </Form.Label>
              <TextField
                margin="dense"
                type="text"
                name="about"
                value={this.state.about}
                onChange={handleChanged}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                רשימת סטודנטים:
              </Form.Label>
              <input
                className={classes.input}
                type="file"
                onChange={changeHandler}
                style={{ padding: "10px" }}
                name="FileName"
                id="contained-button-file"
              />
              <label htmlFor="contained-button-file">
                <Button variant="contained" color="default" component="span">
                  הוספת רשימה
                </Button>
              </label>
              <h7>{this.state.FileName.split("C:\\fakepath\\")}</h7>
            </Form.Row>
            <Form.Row>
              <h6 className={classes.note}>
                {" "}
                מרצה שים לב! על הקובץ להיות בפורמט הבא:{" "}
              </h6>
            </Form.Row>
            <Form.Row>
              <img
                className={classes.image}
                src={process.env.PUBLIC_URL + "/excelFormatExample.JPG"}
                width="370px"
                alt=""
              />{" "}
            </Form.Row>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => cancelAction()} color="primary">
              ביטול
            </Button>
            <Button onClick={addCourse} color="primary">
              יצירת קורס
            </Button>
          </DialogActions>

          <Snackbar open={this.state.errorsSnackbar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() => this.setState({ errorsSnackbar: false })}
            >
              {this.state.errList}
            </Alert>
          </Snackbar>
          <Snackbar
            open={this.state.courseAddedSnackbar}
            autoHideDuration={6000}
          >
            <Alert
              severity="success"
              onClose={() =>
                this.setState({ courseAddedSnackbar: false }, onClose)
              }
            >
              הקורס נוצר בהצלחה
            </Alert>
          </Snackbar>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(AddCourseDialog);
