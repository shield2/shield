import React from "react";
import axios from "axios";
import { styles } from "./AdminHome.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Form, Col } from "react-bootstrap";
import { Button, TextField, withStyles } from "@material-ui/core";
import { ExcelRenderer } from "react-excel-renderer";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import CheckIcon from "@material-ui/icons/Check";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class EditCourseDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      courseName: "",
      year: "",
      semester: "",
      minStudents: "",
      maxStudents: "",
      about: "",
      rows: [],
      cols: [],
      FileName: "",
      studentId: "",
      add: [],
      btn: [1],
      studentExistSnackbar: false,
      newUserSnackbar: false,
      studentAddedSnackbar: false,
      errorsSnackbar: false,
      successSnackbar: false,
      errList: "",
    };
  }

  componentDidMount() {
    this.getCourseDetails();
  }

  getCourseDetails() {
    axios
      .get(`/api/getCourseByID?id=${this.props.courseId}`)
      .then((res) => {
        const course = res.data;
        // console.log("get course details");
        // console.log(course);
        this.setState({
          courseName: course.name,
          year: course.year,
          semester: course.semester,
          minStudents: course.minStudents,
          maxStudents: course.maxStudents,
          about: course.description,
        });
      })
      .catch((err) => {
        // console.log("get course error");
      });
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;

    const handleChanged = (e) => {
      this.setState({ [e.target.name]: e.target.value });
    };

    const checkFileFormat = () => {
      if (this.state.FileName !== "") {
        var fileTitles = this.state.rows[0];
        return (
          fileTitles[0] === "שם פרטי" &&
          fileTitles[1] === "שם משפחה" &&
          fileTitles[3] === "תעודת זהות" &&
          fileTitles[2] === "מייל" &&
          fileTitles.length === 4
        );
      }

      return 1;
    };

    const parseRowsToUsersArray = () => {
      var stList = [];
      var stRows = this.state.rows.slice(1);

      stRows.forEach((st) => {
        var currStudent = {
          firstName: st[0],
          lastName: st[1],
          id: st[3],
          mail: st[2],
          isAdmin: false,
        };
        stList.push(currStudent);
      });

      return stList;
    };

    const updateCourse = () => {
      var data = {
        id: this.props.courseId,
        courseName: this.state.courseName,
        year: this.state.year,
        semester: this.state.semester,
        minStudents: this.state.minStudents,
        maxStudents: this.state.maxStudents,
        about: this.state.about,
      };

      var FileNameSplits = this.state.FileName.split(".");
      var filePrefix = FileNameSplits[FileNameSplits.length - 1];
      var text = /^[0-9]+$/;

      var errors = [];
      if (data.courseName === "") errors.push("לא הוזן שם הקורס");
      if (data.year === "") errors.push("לא הוזנה השנה בה מתקיים הקורס");
      if ((data.year !== "") && ((data.year.length !== 4) || (!text.test(data.year)))) errors.push("יש להזין שנה בפורמט YYYY מספרים בלבד");
      if (data.semester === "") errors.push("לא הוזן הסמסטר בו מתקיים הקורס");
      if (data.minStudents === "")
        errors.push("לא הוזן מספר מינימאלי של סטודנטים בקבוצה");
      if (data.maxStudents === "")
        errors.push("לא הוזן מספר מקסימלי של סטודנטים בקבוצה");
      if (data.maxStudents < data.minStudents)
        errors.push("מספר סטודנטים מקסימלי קטן מהמספר המינימאלי");
      if (filePrefix !== "" && filePrefix !== "xlsx" && filePrefix !== "xls")
        errors.push("קובץ הסטודנטים שנטען אינו בפורמט xls/xlsx כנדרש");
      if (!checkFileFormat())
        errors.push("עמודת המידע בקובץ הסטודנטים אינו בפורמט הנדרש");

      if (errors.length === 0) {
        var stList = "";

        if (this.state.FileName !== "") {
          stList = parseRowsToUsersArray();
        }

        data = { ...data, studentsList: stList };

        axios
          .post(`/api/updateCourse`, data)
          .then((res) => {
            // console.log(res);
            // console.log("success");
            this.setState({ successSnackbar: true });
          })
          .catch((err) => {
            if (err.response.status === 400)
              //alert(err.response.data['err'])
              this.setState({ errList: "שגיאה", errorsSnackbar: true });
            // console.log("Something went wrong");
          });

        this.setState({
          courseName: this.state.courseName,
          year: this.state.year,
          semester: this.state.semester,
          minStudents: this.state.minStudents,
          maxStudents: this.state.maxStudents,
          about: this.state.about,
          FileName: this.state.FileName,
        });

      } else {
        this.setState({ errList: errors.join(", "), errorsSnackbar: true });
        //alert(errors.join("\n"));
      }
    };

    const changeHandler = (event) => {
      let fileObj = event.target.files[0];
      this.setState({ [event.target.name]: event.target.value });

      if (fileObj) {
        //just pass the fileObj as parameter
        ExcelRenderer(fileObj, (err, resp) => {
          if (err) {
            // console.log(err);
          } else {
            // console.log(resp);
            this.setState({
              cols: resp.cols,
              rows: resp.rows,
            });
            // console.log(this.state);
          }
        });
      }
    };

    const addStudentField = (event) => {
      this.setState({
        add: [1],
        btn: [],
      });
      event.preventDefault();
    };

    const deleteStudentField = () => {
      this.setState({
        add: [],
        btn: [1],
        studentId: "",
      });
    };

    const addStudent = () => {
      var studentIsRegister;
      var data = { uId: this.state.studentId };

      axios
        .post(`/api/getCoursesOfStudent`, data, {
          withCredentials: true,
        })
        .then((res) => {
          studentIsRegister = res.data.find((course) => {
            return course._id === this.props.courseId;
          });

          if (studentIsRegister) {
            this.setState({ studentExistSnackbar: true });
            deleteStudentField();
          } else {
            axios
              .get(`/api/getUserById?id=` + this.state.studentId,
                { withCredentials: true }
              )
              .then((res) => {
                if (!res.data.id) {
                  this.setState({ newUserSnackbar: true });
                  deleteStudentField();
                } else {
                  var data = {
                    id: this.props.courseId,
                    studentId: this.state.studentId,
                  };

                  axios
                    .post(`/api/addStudentToCourse`, data)
                    .then((res) => {
                      // console.log("success");
                      this.setState({ studentAddedSnackbar: true });
                      deleteStudentField();
                      //onClose();
                    })
                    .catch((err) => {
                      // console.log("Something went wrong");
                    });
                }
              })
              .catch((err) => {
                // console.log("get user error");
              });
          }
        })
        .catch((err) => {
          // console.log("get courses error");
        });
    };

    const handleClose = (event, reason) => {
      if (reason === "clickaway") {
        return;
      }

      this.setState({
        studentExistSnackbar: false,
        newUserSnackbar: false,
        studentAddedSnackbar: false,
      });
    };

    return (
      <div className={classes.root}>
        <Dialog
          open={isOpen}
          onClose={onClose}
          aria-labelledby="form-dialog-title"
          className={classes.dialog}
          maxWidth="xl"
        >
          <DialogTitle id="form-dialog-title">עריכת פרטי קורס</DialogTitle>
          <DialogContent>
            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                שם הקורס:
              </Form.Label>
              <TextField
                required
                autoFocus
                margin="dense"
                type="text"
                name="courseName"
                value={this.state.courseName}
                onChange={handleChanged}
              />
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                שנה:
              </Form.Label>
              <TextField
                autoFocus
                margin="dense"
                type="text"
                name="year"
                value={this.state.year}
                onChange={handleChanged}
              />
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                סמסטר:
              </Form.Label>
              <Col sm="5">
                <Form.Control
                  as="select"
                  value={this.state.semester}
                  onChange={handleChanged}
                  name="semester"
                >
                  <option> </option>
                  <option>א</option>
                  <option>ב</option>
                  <option>קיץ</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                מינימום סטודנטים בקבוצה:
              </Form.Label>
              <Col sm="5">
                <Form.Control
                  as="select"
                  value={this.state.minStudents}
                  onChange={handleChanged}
                  name="minStudents"
                >
                  <option> </option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                מקסימום סטודנטים בקבוצה:
              </Form.Label>
              <Col sm="5">
                <Form.Control
                  as="select"
                  value={this.state.maxStudents}
                  onChange={handleChanged}
                  name="maxStudents"
                >
                  <option> </option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                  <option>7</option>
                  <option>8</option>
                </Form.Control>
              </Col>
            </Form.Row>
            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                תיאור:
              </Form.Label>
              <TextField
                autoFocus
                margin="dense"
                type="text"
                name="about"
                value={this.state.about}
                onChange={handleChanged}
              />
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                רשימת סטודנטים: {this.state.FileName.split("C:\\fakepath\\")}
              </Form.Label>
              <input
                // accept="/*"
                className={classes.input}
                id="contained-button-file"
                multiple
                type="file"
                display="none"
                onChange={changeHandler}
                name="FileName"
              />
              <label htmlFor="contained-button-file">
                <Button
                  id="contained-button-file"
                  variant="contained"
                  color="default"
                  component="span"
                >
                  <GroupAddIcon />
                  החלפת רשימה
                </Button>
              </label>
              {this.state.btn.map((student) => {
                return (
                  <Button
                    variant="contained"
                    color="default"
                    component="span"
                    className={classes.addStudentBtn}
                    onClick={addStudentField}
                  >
                    <PersonAddIcon />
                    הוספת סטודנט
                  </Button>
                );
              })}
            </Form.Row>
            <Form.Row>
              {this.state.add.map((student) => {
                return (
                  <Form.Row>
                    <Form.Label column sm="4" className={classes.dialogLable}>
                      תעודת זהות סטודנט:
                    </Form.Label>
                    <TextField
                      autoFocus
                      margin="dense"
                      type="text"
                      name="studentId"
                      value={this.state.studentId}
                      onChange={handleChanged}
                    />
                    <Form.Label column sm="4" className={classes.btn}>
                      <IconButton
                        aria-label="delete"
                        onClick={deleteStudentField}
                      >
                        <DeleteIcon fontSize="small" />
                      </IconButton>
                      <IconButton aria-label="delete" onClick={addStudent}>
                        <CheckIcon fontSize="small" />
                      </IconButton>
                    </Form.Label>
                  </Form.Row>
                );
              })}
            </Form.Row>
            <Form.Row>
              <h6 className={classes.note}>
                מרצה שים לב! על הקובץ להיות בפורמט הבא:
              </h6>
            </Form.Row>
            <Form.Row>
              <img
                className={classes.image}
                src={process.env.PUBLIC_URL + "/excelFormatExample.JPG"}
                width="370px"
                alt=""
              />{" "}
            </Form.Row>
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose} color="primary">
              ביטול
            </Button>
            <Button onClick={updateCourse} color="primary">
              עדכון קורס
            </Button>
          </DialogActions>

          <Snackbar
            open={this.state.studentExistSnackbar}
            autoHideDuration={6000}
            onClose={handleClose}
          >
            <Alert onClose={handleClose} severity="info">
              הסטודנט כבר רשום לקורס
            </Alert>
          </Snackbar>
          <Snackbar
            open={this.state.newUserSnackbar}
            autoHideDuration={6000}
            onClose={handleClose}
          >
            <Alert onClose={handleClose} severity="error">
              משתמש אינו קיים - יש ליצור משתמש חדש
            </Alert>
          </Snackbar>
          <Snackbar
            open={this.state.studentAddedSnackbar}
            autoHideDuration={6000}
            onClose={handleClose}
          >
            <Alert onClose={handleClose} severity="success">
              הסטודנט נוסף בהצלחה
            </Alert>
          </Snackbar>
          <Snackbar open={this.state.errorsSnackbar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() => this.setState({ errorsSnackbar: false })}
            >
              {this.state.errList}
            </Alert>
          </Snackbar>
          <Snackbar open={this.state.successSnackbar} autoHideDuration={6000}>
            <Alert
              severity="success"
              onClose={() => this.setState({ successSnackbar: false }, onClose)}
            >
              הקורס עודכן בהצלחה
            </Alert>
          </Snackbar>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(EditCourseDialog);
