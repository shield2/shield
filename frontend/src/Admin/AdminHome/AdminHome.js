import React from 'react';
import { styles } from './AdminHome.css.js';
import { withStyles, Card, CardContent, Typography, CardActions, Grid, TextField, Fab, Tooltip }
  from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import BallotIcon from '@material-ui/icons/Ballot';
import { Form } from "react-bootstrap";
import axios from 'axios';
import AddCourseDialog from "./AddCourse.js"
import EditCourseDialog from "./EditCourse.js"
import DeleteCourseDialog from "./DeleteCourse.js"
import ViewStudentsDialog from "./ViewStudents.js"
import { navigate } from "@reach/router";
import PeopleRoundedIcon from '@material-ui/icons/PeopleRounded';

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 17,
  },
}))(Tooltip);

class AdminHome extends React.Component {
  constructor(props) {
    super(props);
    // console.log(props)
    this.state = {
      allCourses: [],
      currCourse: "",
      studentsList: [],
      name: "",
      year: "",
      semester: "",
      userId: this.props.id,
      userName: this.props.name,

      addCourseDialogOpen: false, // New course dialog open state
      editCourseDialogOpen: false, // Delete course dialog open state
      DeleteCourseDialogOpen: false, // Edit course dialog open state
      ViewStudentsDialogOpen: false,
    };
  }

  componentDidMount() {
    this.getCourses();
  }

  getCourses() {
    // console.log('in getCourses')
    axios.get(`/api/allCoursesOfLecturer?id=${this.state.userId}`).then(res => {
      this.setState({ allCourses: res.data });
      // console.log(res.data)
      // console.log("get courses")
    }).catch(err => {
      // console.log("get courses error")
    })
  }

  render() {
    const { classes } = this.props
    this.getCourses.bind(this)

    const handleAddCourseOpen = () => {
      this.setState({ addCourseDialogOpen: true });
    };

    const handleAddCourseClose = () => {
      this.setState({ addCourseDialogOpen: false });
      this.getCourses();
    };

    const handleEditCourseOpen = CourseToUpdate => {
      this.setState({ editCourseDialogOpen: true });
      this.setState({ currCourse: CourseToUpdate });
    };

    const handleEditCourseClose = () => {
      this.setState({ editCourseDialogOpen: false });
      this.getCourses();
    };

    const handleViewStudentsOpen = (CourseToView, students) => {
      this.setState({ ViewStudentsDialogOpen: true });
      this.setState({
        currCourse: CourseToView,
        studentsList: students
      });
    };

    const handleViewStudentsClose = () => {
      this.setState({ ViewStudentsDialogOpen: false });
      this.getCourses();
    };

    const handleClickOpen = CourseToDelete => {
      this.setState({ DeleteCourseDialogOpen: true });
      this.setState({ currCourse: CourseToDelete });
    };

    const handleClose = () => {
      this.setState({ DeleteCourseDialogOpen: false });
      this.getCourses();
    };

    const handleCourseChange = e => {
      this.setState({
        name: e.target.value
      })
    }

    const handleYearChange = e => {
      this.setState({
        year: e.target.value
      })
    }

    const handleSemesterChange = e => {
      this.setState({
        semester: e.target.value
      })
    }

    return (
      <div className={classes.root}>
        <h1 className={classes.mainTitle}>הקורסים שלי</h1>
        <Form.Row>
          <TextField className={classes.search} label="שם הקורס" onChange={handleCourseChange} />
          <TextField className={classes.search} label="שנה" onChange={handleYearChange} />
          <TextField className={classes.search} label="סמסטר" onChange={handleSemesterChange} />
        </Form.Row>

        <Tooltip title="קורס חדש" aria-label="add">
          <Fab
            className={classes.btnAdd}
            aria-label="add"
            type="button"
            size="small"
            onClick={handleAddCourseOpen}
          >
            <AddIcon />
          </Fab>
        </Tooltip>
        <Grid container spacing={2}>
          {this.state.allCourses.filter(course => {
            return (course.name.includes(this.state.name) &&
              course.year.includes(this.state.year) &&
              course.semester.includes(this.state.semester))
          }).map((elem, index) => (
            <Grid key={this.state.allCourses.indexOf(elem)}>
              <Card className={classes.card}>
                <CardActions>
                  <Tooltip title="רשימת סטודנטים" aria-label="registers">
                    <Fab variant="extended" color="inherit" size="small" onClick={() => handleViewStudentsOpen(elem._id, elem.students)}>
                      <PeopleRoundedIcon color="action" />
                    </Fab>
                  </Tooltip>
                </CardActions>
                <CardContent>
                  <LightTooltip title={elem.name} placement="top-end">

                    <Typography
                      className={classes.text}
                      gutterBottom
                      variant="h5"
                      component="h2"
                      noWrap
                    >
                      {elem.name}
                    </Typography>
                  </LightTooltip>

                  <Typography
                    className={classes.text}
                    variant="h6"
                    color="textSecondary"
                    component="p"
                  >
                    {elem.year + " " + elem.semester}
                  </Typography>
                  <Typography
                    className={classes.text}
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    מרצה: {elem.lecturer ?? '...'}
                  </Typography>
                </CardContent>
                <CardActions className={classes.cardButton}>
                  <Tooltip title="מחיקה" aria-label="delete">
                    <Fab
                      className={classes.deleteBtn}
                      size="small"
                      aria-label="delete"
                      onClick={() => handleClickOpen(elem._id)}
                    >
                      <DeleteIcon />
                    </Fab>
                  </Tooltip>
                  <Tooltip title="עריכה" aria-label="edit">
                    <Fab
                      className={classes.editBtn}
                      size="small"
                      aria-label="edit"
                      onClick={() => handleEditCourseOpen(elem._id)}
                    >
                      <EditIcon />
                    </Fab>
                  </Tooltip>
                  <Tooltip title="הגנות" aria-label="PresentDefenses">
                    <Fab
                      className={classes.btnList}
                      size="small"
                      aria-label="PresentDefenses"
                      onClick={() => navigate(`/Defenses/ShowDefenses/${elem._id}`)}
                    >
                      <BallotIcon />
                    </Fab>
                  </Tooltip>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>

        <AddCourseDialog open={this.state.addCourseDialogOpen} onClose={handleAddCourseClose} uid={this.state.userId} uname={this.state.userName} />
        <DeleteCourseDialog open={this.state.DeleteCourseDialogOpen} onClose={handleClose} id={this.state.currCourse} />
        {this.state.editCourseDialogOpen ? <EditCourseDialog open onClose={handleEditCourseClose} courseId={this.state.currCourse} /> : ""}
        <ViewStudentsDialog open={this.state.ViewStudentsDialogOpen} onClose={handleViewStudentsClose}
          id={this.state.currCourse} students={this.state.studentsList} />


      </div>
    )
  }
}

export default withStyles(styles)(AdminHome)