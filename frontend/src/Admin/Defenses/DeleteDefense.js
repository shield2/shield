import React from "react";
import axios from "axios";
import { styles } from "../AdminHome/AdminHome.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  Button,
  withStyles,
} from "@material-ui/core";

class DeleteDefenseDialog extends React.Component {

  constructor(props) {
    super();
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;

    const deleteDefense = () => {
      var data = {
        id: this.props.id
      };

      axios.post(`/api/deleteDefense`, data).then(res => {
        // console.log("success")
        onClose();
      }).catch(err => {
        // console.log("error")
        onClose();
      })
    }

    return (
      <div className={classes.root}>
        <Dialog
          className={classes.dialog}
          open={isOpen}
          onClose={onClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"אישור מחיקה"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              האם אתה בטוח שברצונך למחוק את ההגנה?
                          </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={deleteDefense} className={classes.deleteBtn}>
              מחיקה
                          </Button>
            <Button onClick={onClose} className={classes.CancelBtn}>
              ביטול
                          </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default withStyles(styles)(DeleteDefenseDialog)