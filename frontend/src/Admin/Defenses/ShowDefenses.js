import React from 'react';
import { styles } from '../AdminHome/AdminHome.css.js';
import { withStyles, Card, CardContent, Typography, CardActions, Grid, Fab, Tooltip }
  from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import EventNoteIcon from "@material-ui/icons/EventNote";
import AddIcon from "@material-ui/icons/Add";
import AddDefenseDialog from "./AddDefense.js"
import EditDefenseDialog from "./EditDefense.js"
import DeleteDefenseDialog from "./DeleteDefense.js"
import ViewDefenseDialog from "./ViewDefenseRegisters.js"
import axios from 'axios';

class ShowDefenses extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      defenses: [],
      addDefenseDialogOpen: false, // New defense dialog open state
      editDefenseDialogOpen: false, // Delete defense dialog open state
      DeleteDefenseDialogOpen: false, // Edit defense dialog open state
      ViewDefenseRegistersDialogOpen: false,
      currDefense: "",
      date: "",
      time: "",
      startTime: "",
      endTime: "",
    };
  }

  componentDidMount() {
    this.getDefenses();
  }

  getDefenses() {
    axios.get(`/api/getDefenses?id=` + this.props.courseId).then(res => {
      this.setState({ defenses: res.data });
      // console.log(res.data)
    }).catch(err => {
      // console.log("get defenses error")
    })
  }

  render() {
    const { classes } = this.props

    const handleAddDefenseOpen = () => {
      this.setState({ addDefenseDialogOpen: true });
    };

    const handleAddDefenseClose = () => {
      this.setState({ addDefenseDialogOpen: false });
      this.getDefenses();
    };

    const handleEditDefenseOpen = (id, date, time, sTime, eTime) => {
      this.setState({ editDefenseDialogOpen: true });
      this.setState({
        currDefense: id,
        date: date,
        time: time,
        startTime: sTime,
        endTime: eTime
      });
    };

    const handleEditDefenseClose = () => {
      this.setState({ editDefenseDialogOpen: false });
      this.getDefenses();
    };

    const handleDeleteDefenseOpen = DefenseToDelete => {
      this.setState({ DeleteDefenseDialogOpen: true });
      this.setState({ currDefense: DefenseToDelete });
    };

    const handleDeleteDefenseClose = () => {
      this.setState({ DeleteDefenseDialogOpen: false });
      this.getDefenses();
    };

    const handleViewDefenseRegistersOpen = (DefenseToView, date, time) => {
      this.setState({ ViewDefenseRegistersDialogOpen: true });
      this.setState({
        currDefense: DefenseToView,
        date: date,
        time: time
      });
    };

    const handleViewDefenseRegistersClose = () => {
      this.setState({ ViewDefenseRegistersDialogOpen: false });
    };

    return (
      <div className={classes.root}>
        <h1 className={classes.mainTitle}> הגנות לקורס </h1>

        <Tooltip title="הגנה חדשה" aria-label="add">
          <Fab
            className={classes.btnAdd}
            aria-label="add"
            type="button"
            size="small"
            onClick={handleAddDefenseOpen}
          >
            <AddIcon />
          </Fab>
        </Tooltip>

        <Grid container spacing={2}>
          {this.state.defenses.map(elem =>
            <Grid item key={this.state.defenses.indexOf(elem)}>
              <Card className={classes.defenseCard}>
                <CardActions >
                  <Tooltip title="הרשמות" aria-label="registers">
                    {/* <IconButton aria-label="registers" size="small" onClick={() => navigate(`/Defenses/ShowDefenses/${elem._id}`)}>
                        <EventNoteIcon />
                      </IconButton> */}
                    <Fab variant="extended" color="inherit" size="small" onClick={() => handleViewDefenseRegistersOpen(elem._id, elem.date, elem.time)}>
                      <EventNoteIcon color="action" />
                    </Fab>
                  </Tooltip>
                </CardActions>
                <CardContent>

                  <Typography className={classes.text} gutterBottom variant="body1" component="h2">
                    תאריך: {elem.date}
                  </Typography>
                  <Typography className={classes.text} variant="body2" color="textSecondary" component="p">
                    שעת התחלה: {elem.startTime}
                  </Typography>
                  <Typography className={classes.text} variant="body2" color="textSecondary" component="p">
                    שעת סיום: {elem.endTime}
                  </Typography>

                </CardContent>
                <CardActions className={classes.cardButton}>
                  <Tooltip title="מחיקה" aria-label="delete">
                    <Fab
                      className={classes.deleteBtn}
                      size="small"
                      aria-label="delete"
                      onClick={() => handleDeleteDefenseOpen(elem._id)}
                    >
                      <DeleteIcon />
                    </Fab>
                  </Tooltip>
                  <Tooltip title="עריכה" aria-label="edit">
                    <Fab
                      className={classes.editBtn}
                      size="small"
                      aria-label="edit"
                      onClick={() => handleEditDefenseOpen(elem._id, elem.date, elem.time, elem.startTime, elem.endTime)}
                    >
                      <EditIcon />
                    </Fab>
                  </Tooltip>
                </CardActions>
              </Card>
            </Grid>
          )}
        </Grid>

        <AddDefenseDialog open={this.state.addDefenseDialogOpen} onClose={handleAddDefenseClose} id={this.props.courseId} />
        <DeleteDefenseDialog open={this.state.DeleteDefenseDialogOpen} onClose={handleDeleteDefenseClose} id={this.state.currDefense} />
        {this.state.editDefenseDialogOpen ?
          <EditDefenseDialog
            open={this.state.editDefenseDialogOpen}
            onClose={handleEditDefenseClose}
            defenseId={this.state.currDefense}
            date={this.state.date}
            time={this.state.time}
            startTime={this.state.startTime}
            endTime={this.state.endTime}
          /> : ""}
        <ViewDefenseDialog open={this.state.ViewDefenseRegistersDialogOpen} onClose={handleViewDefenseRegistersClose}
          id={this.state.currDefense} date={this.state.date} time={this.state.time} requestTime={new Date()} />

      </div>
    );
  }

}

export default withStyles(styles)(ShowDefenses)