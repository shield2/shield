import React from "react";
import axios from "axios";
import { styles } from "../AdminHome/AdminHome.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import {
  Button,
  withStyles,
} from "@material-ui/core";
import Chip from '@material-ui/core/Chip';
import Tooltip from '@material-ui/core/Tooltip';
import { CSVLink } from 'react-csv'



class ViewDefenseDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      registers: [],
      allUsers: [],
      count: 0,
      csvData: []
    };
  }

  componentDidUpdate(prevProps) {
    if ((prevProps.id !== this.props.id) || (prevProps.requestTime !== this.props.requestTime)) {
      this.getDefensRegisters();
    }

    this.getUsers();
  }

  getDefensRegisters() {
    axios.get(`/api/getDefenseRegisters?id=` + this.props.id).then(res => {
      this.setState({ registers: res.data });
      // console.log(res.data)
      var count = 0;
      var data = [];
      res.data.map(register => {
        if (register.groupMembers.length === 0) {
          count++;
        }

        var students = ""

        register.groupMembers.map(id => {
          return (students += "  " + id.toString())
        })

        data.push({ Group: register.hour, Students: students })
        return null
      })

      this.setState({
        count: count,
        csvData: data
      })

    }).catch(err => {
      // console.log("get defenses error")
    })
  }

  getUsers() {
    // console.log('in getUsers')
    axios.get(`/api/allUsers`, { withCredentials: true }).then(res => {
      this.setState({ allUsers: res.data })
      // console.log(res.data)
      // console.log("get users")
    }).catch(err => {
      // console.log("get courses error")
    })
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;

    return (
      <div className={classes.root}>
        <Dialog
          open={isOpen}
          onClose={onClose}
          aria-labelledby="form-dialog-title"
          className={classes.dialog}
        >
          <DialogTitle id="form-dialog-title">רשומים להגנה</DialogTitle>

          <DialogContent>
            <h6>
              מועד ההגנה: {this.props.date}
            </h6>
            <h6>
              זמן הגנה: {this.props.time} דקות
          </h6>
            <br />
            <h7>
              הגנות פנויות: {this.state.count}
            </h7>
            <DialogActions>
              <Button>
                <CSVLink data={this.state.csvData} filename={this.props.date + ".csv"}>ייצוא ל CSV</CSVLink>
              </Button>
            </DialogActions>

            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <TableRow background="#eeeeee">
                    <TableCell align="right">מספר קבוצה</TableCell>
                    <TableCell align="right">סטודנטים</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.registers.sort((a, b) => a.hour - b.hour).map(u => (
                    <TableRow key={u.id}>
                      <TableCell align="right">{u.hour}</TableCell>
                      <TableCell align="left">
                        {u.groupMembers.map(studentId => {
                          const student = this.state.allUsers.find(user => (user.id === studentId))

                          return (
                            <Tooltip title={<span>
                              תעודת זהות: {student.id}
                              <br />
                                        מייל: {student.mail}
                            </span>}
                              //style={{ fontSize: "20px", color: "blue" }}
                              placement="top">
                              <Chip
                                size="small"
                                //avatar={<Avatar>M</Avatar>}
                                //icon={icon}
                                label={student.firstName + ' ' + student.lastName}
                                //onDelete={data.label === 'React' ? undefined : handleDelete(data)}
                                //onClick={}
                                //href="#chip" 
                                clickable
                              //variant="outlined"
                              />
                            </Tooltip>
                          );
                        })}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose} color="primary">
              סגור
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(ViewDefenseDialog);
