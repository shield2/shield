import React from "react";
import axios from "axios";
import { styles } from "../AdminHome/AdminHome.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Form, Col } from "react-bootstrap";
import { Button, withStyles, Grid, Input, TextField } from "@material-ui/core";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class AddDefenseDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      courseId: this.props.id,
      date: new Date(),
      time: 15,
      startTime: "07:30",
      endTime: "10:30",
      defenseAddedSnackbar: false,
      errorsSnackbar: false,
      errList: "",
    };
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;

    const handleDateChange = (date) => {
      this.setState({ date: date });
    };

    const cancelAction = () => {
      this.setState({
        courseId: this.props.id,
        date: new Date(),
        time: 15,
        startTime: "07:30",
        endTime: "10:30",
        defenseAddedSnackbar: false,
        errorsSnackbar: false,
        errList: ""
      }, onClose)
    }

    const handleChanged = (e) => {
      this.setState({ [e.target.name]: e.target.value });
    };

    const handleBlur = (value) => {
      if (value < 0) {
        this.setState({ time: 0 });
      } else if (value > 60) {
        this.setState({ time: 60 });
      }
    };

    const addDefense = () => {
      var data = {
        courseId: this.state.courseId,
        date: new Intl.DateTimeFormat("en-GB", {
          year: "numeric",
          month: "2-digit",
          day: "2-digit",
        }).format(this.state.date),
        time: this.state.time,
        startTime: this.state.startTime,
        endTime: this.state.endTime,
      };

      var startTime = new Date("01/01/1970" + " " + data.startTime);
      var endTime = new Date("01/01/1970" + " " + data.endTime);

      var errors = [];
      if (data.date === "01/01/1970") errors.push("לא נבחר תאריך להגנה");
      if (data.time === "") errors.push("לא נבחר זמן להגנה");
      if (data.startTime === "") errors.push("לא נבחרה שעת התחלה");
      if (data.endTime === "") errors.push("לא נבחרה שעת סיום");
      if (endTime < startTime) errors.push("שעת הסיום שנבחרה לפני שעת ההתחלה");

      if (errors.length === 0) {
        axios
          .post(`/api/addDefense`, data)
          .then((res) => {
            // console.log(res)
            // console.log("success")
            this.setState({ defenseAddedSnackbar: true });
          })
          .catch((err) => {
            if (err.response.status === 400)
              this.setState({ errList: "שגיאה", errorsSnackbar: true });
            // console.log("Something went wrong")
          });
      } else {
        this.setState({ errList: errors.join(", "), errorsSnackbar: true });
        //alert(errors.join("\n"));
      }

      if (this.state.defenseAddedSnackbar)
        this.setState({
          date: new Date(),
          time: 15,
          startTime: "07:30",
          endTime: "10:30",
        });
    };

    return (
      <div className={classes.root}>
        <Dialog
          open={isOpen}
          onClose={() => cancelAction()}
          aria-labelledby="form-dialog-title"
          className={classes.dialog}
        >
          <DialogTitle id="form-dialog-title">הוספת הגנה חדשה</DialogTitle>
          <DialogContent>
            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                תאריך ההגנה:
              </Form.Label>
              <Col sm="20">
                <DatePicker
                  selected={this.state.date}
                  onChange={handleDateChange}
                  dateFormat="dd/MM/yyyy"
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                זמן הגנה(דקות):
              </Form.Label>
              <Col sm="20">
                <Grid item>
                  <Input
                    defaultValue={this.state.time}
                    value={this.state.time}
                    margin="dense"
                    name="time"
                    onChange={handleChanged}
                    onBlur={handleBlur(this.state.time)}
                    inputProps={{
                      step: 1,
                      min: 0,
                      max: 60,
                      type: "number",
                      "aria-labelledby": "input-slider",
                    }}
                  />
                </Grid>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                שעת התחלה:
              </Form.Label>
              <TextField
                id="time"
                type="time"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  step: 300, // 5 min
                }}
                value={this.state.startTime}
                name="startTime"
                onChange={handleChanged}
              />
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                שעת סיום:
              </Form.Label>
              <TextField
                id="time"
                type="time"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  step: 300, // 5 min
                }}
                value={this.state.endTime}
                name="endTime"
                onChange={handleChanged}
              />
            </Form.Row>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => cancelAction()} color="primary">
              ביטול
            </Button>
            <Button onClick={addDefense} color="primary">
              יצירת הגנה
            </Button>
          </DialogActions>

          <Snackbar open={this.state.errorsSnackbar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() => this.setState({ errorsSnackbar: false })}
            >
              {this.state.errList}
            </Alert>
          </Snackbar>
          <Snackbar
            open={this.state.defenseAddedSnackbar}
            autoHideDuration={6000}
          >
            <Alert
              severity="success"
              onClose={() =>
                this.setState({ defenseAddedSnackbar: false }, onClose)
              }
            >
              ההגנה נוצרה בהצלחה
            </Alert>
          </Snackbar>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(AddDefenseDialog);
