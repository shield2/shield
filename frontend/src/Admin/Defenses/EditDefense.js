import React from "react";
import axios from "axios";
import { styles } from "../AdminHome/AdminHome.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Form, Col } from "react-bootstrap";
import { Button, withStyles, Grid, Input, TextField } from "@material-ui/core";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


class EditDefenseDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      date: "",
      time: this.props.time,
      startTime: this.props.startTime,
      endTime: this.props.endTime,
      oldStartTime: this.props.startTime,
      oldDate: "",
      errorsSnackbar: false,
      successSnackbar: false,
      errList: ""
    };
  }

  componentDidMount() {
    var dateMomentObject = moment(this.props.date, "DD/MM/YYYY");
    var dateObject = dateMomentObject.toDate();

    this.setState({
      date: dateObject,
      time: this.props.time,
      startTime: this.props.startTime,
      endTime: this.props.endTime,
      oldDate: dateObject,
      oldStartTime: this.props.startTime,
    });
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;

    const handleDateChange = (date) => {
      this.setState({ date: date });
    };

    const handleChanged = (e) => {
      this.setState({ [e.target.name]: e.target.value });
    };

    const handleBlur = (value) => {
      if (value < 0) {
        this.setState({ time: 0 });
      } else if (value > 60) {
        this.setState({ time: 60 });
      }
    };

    const updateDefense = () => {
      var data = {
        id: this.props.defenseId,
        date: new Intl.DateTimeFormat("en-GB", {
          year: "numeric",
          month: "2-digit",
          day: "2-digit",
        }).format(this.state.date),
        time: this.state.time,
        startTime: this.state.startTime,
        endTime: this.state.endTime,
        oldStartTime: this.state.oldStartTime,
        oldDate: new Intl.DateTimeFormat("en-GB", {
          year: "numeric",
          month: "2-digit",
          day: "2-digit",
        }).format(this.state.oldDate),
      };

      var startTime = new Date("01/01/1970" + " " + data.startTime);
      var endTime = new Date("01/01/1970" + " " + data.endTime);

      var errors = [];
      if (data.date === "01/01/1970") errors.push("לא נבחר תאריך להגנה");
      if (data.time === "") errors.push("לא נבחר זמן להגנה");
      if (data.startTime === "") errors.push("לא נבחרה שעת התחלה");
      if (data.endTime === "") errors.push("לא נבחרה שעת סיום");
      if (endTime < startTime) errors.push("שעת הסיום שנבחרה לפני שעת ההתחלה")

      if (errors.length === 0) {
        // console.log(data);
        axios
          .post(`/api/updateDefense`, data)
          .then((res) => {
            // console.log(res);
            // console.log("success");
            this.setState({ successSnackbar: true })
          })
          .catch((err) => {
            if (err.response.status === 400)
              this.setState({ errList: "שגיאה", errorsSnackbar: true });
            // console.log("Something went wrong");
          });
      } else {
        this.setState({ errList: errors.join(", "), errorsSnackbar: true })
        //alert(errors.join("\n"));
      }

      this.setState({
        date: this.state.date,
        time: this.state.time,
        startTime: this.state.startTime,
        endTime: this.state.endTime,
      });
    };

    return (
      <div className={classes.root}>
        <Dialog
          open={isOpen}
          onClose={onClose}
          aria-labelledby="form-dialog-title"
          className={classes.dialog}
        >
          <DialogTitle id="form-dialog-title">עריכת פרטי הגנה</DialogTitle>
          <DialogContent>
            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                תאריך ההגנה:
              </Form.Label>
              <Col sm="20">
                <DatePicker
                  selected={this.state.date}
                  onChange={handleDateChange}
                  dateFormat="dd/MM/yyyy"
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                זמן הגנה(דקות):
              </Form.Label>
              <Col sm="20">
                <Grid item>
                  <Input
                    defaultValue={this.state.time}
                    value={this.state.time}
                    margin="dense"
                    name="time"
                    onChange={handleChanged}
                    onBlur={handleBlur(this.state.time)}
                    inputProps={{
                      step: 1,
                      min: 0,
                      max: 60,
                      type: "number",
                      "aria-labelledby": "input-slider",
                    }}
                  />
                </Grid>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                שעת התחלה:
              </Form.Label>
              <TextField
                id="time"
                type="time"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  step: 300, // 5 min
                }}
                value={this.state.startTime}
                name="startTime"
                onChange={handleChanged}
              />
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4" className={classes.dialogLable}>
                שעת סיום:
              </Form.Label>
              <TextField
                id="time"
                type="time"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  step: 300, // 5 min
                }}
                value={this.state.endTime}
                name="endTime"
                onChange={handleChanged}
              />
            </Form.Row>
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose} color="primary">
              ביטול
            </Button>
            <Button onClick={updateDefense} color="primary">
              עדכון הגנה
            </Button>
          </DialogActions>

          <Snackbar open={this.state.errorsSnackbar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() =>
                this.setState({ errorsSnackbar: false })
              }
            >
              {this.state.errList}
            </Alert>
          </Snackbar>
          <Snackbar open={this.state.successSnackbar} autoHideDuration={6000}>
            <Alert
              severity="success"
              onClose={() =>
                this.setState({ successSnackbar: false }, onClose)
              }
            >
              ההגנה עודכנה בהצלחה
            </Alert>
          </Snackbar>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(EditDefenseDialog);
