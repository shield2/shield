import React from "react";
import axios from "axios";
import { styles } from "./UsersStyle.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Form } from "react-bootstrap";
import { Button, TextField, Checkbox, withStyles } from "@material-ui/core";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class AddUserDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      FirstName: "",
      LastName: "",
      Id: "",
      IsAdmin: false,
      mail: "",
      UserExistSnackBar: false,
      UserAddedSnackbar: false,
      FormErrorsSnackbar: false,
      errList: "",
    };
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;
    var mailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const handleChanged = (e) => {
      this.setState({ [e.target.name]: e.target.value });
    };

    const handleChecked = (e) => {
      this.setState({ [e.target.name]: e.target.checked });
    };

    const cancelAction = () => {
      this.setState(
        {
          FirstName: "",
          LastName: "",
          Id: "",
          IsAdmin: false,
          mail: "",
          UserExistSnackBar: false,
          UserAddedSnackbar: false,
          FormErrorsSnackbar: false,
          errList: "",
        },
        onClose
      );
    };

    const addUser = () => {
      var data = {
        firstName: this.state.FirstName,
        lastName: this.state.LastName,
        id: this.state.Id,
        isAdmin: this.state.IsAdmin,
        mail: this.state.mail,
      };

      var errors = [];
      if (data.firstName === "") errors.push("לא הוזן שם פרטי");
      if (data.lastName === "") errors.push("לא הוזן שם משפחה");
      if (data.mail === "") errors.push("לא הוזנה כתובת מייל")
      else if (!mailRegex.test(data.mail)) errors.push("כתובת מייל לא תקינה");
      if (!data.id.match("[0-9]{9}")) errors.push("תעודת זהות לא תקינה");

      if (errors.length === 0) {
        axios
          .post(`/api/addUser`, data, {
            withCredentials: true,
          })
          .then((res) => {
            // console.log(res);
            // console.log("success");
            this.setState({
              UserExistSnackBar: false,
              FormErrorsSnackbar: false,
              UserAddedSnackbar: true,
            });
            this.setState({
              FirstName: "",
              LastName: "",
              Id: "",
              IsAdmin: false,
              mail: "",
            });
          })
          .catch((err) => {
            if (err.response.status === 400)
              this.setState({ UserExistSnackBar: true });
            // console.log("Something went wrong");
            this.setState({
              FirstName: "",
              LastName: "",
              Id: "",
              IsAdmin: false,
            });
          });
      } else {
        this.setState({ errList: errors.join(", "), FormErrorsSnackbar: true });
        //alert(errors.join("\n"));
      }
    };

    return (
      <div className={classes.root}>
        <Dialog
          open={isOpen}
          onClose={() => cancelAction()}
          aria-labelledby="form-dialog-title"
          className={classes.dialog}
        >
          <DialogTitle id="form-dialog-title">הוספת משתמש חדש</DialogTitle>
          <DialogContent>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>שם פרטי:</Form.Label>
              <TextField
                required
                autoFocus
                margin="dense"
                type="text"
                name="FirstName"
                value={this.state.FirstName}
                onChange={handleChanged}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>שם משפחה:</Form.Label>
              <TextField
                autoFocus
                margin="dense"
                type="text"
                name="LastName"
                value={this.state.LastName}
                onChange={handleChanged}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>
                תעודת זהות:
              </Form.Label>
              <TextField
                autoFocus
                margin="dense"
                type="text"
                name="Id"
                value={this.state.Id}
                onChange={handleChanged}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>
                כתובת מייל:
              </Form.Label>
              <TextField
                autoFocus
                margin="dense"
                type="email"
                name="mail"
                value={this.state.mail}
                onChange={handleChanged}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>האם מרצה?</Form.Label>
              <Checkbox
                onChange={handleChecked}
                name="IsAdmin"
                value={this.state.IsAdmin}
                color="primary"
                inputProps={{ "aria-label": "secondary checkbox" }}
              />
            </Form.Row>
          </DialogContent>
          <DialogActions>
            <Button onClick={cancelAction} color="primary">
              ביטול
            </Button>
            <Button onClick={addUser} color="primary">
              יצירת משתמש
            </Button>
          </DialogActions>

          <Snackbar open={this.state.UserExistSnackBar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() =>
                this.setState({ UserExistSnackBar: false }, onClose)
              }
            >
              משתמש עם ת"ז זו כבר קיים במערכת
            </Alert>
          </Snackbar>
          <Snackbar open={this.state.UserAddedSnackbar} autoHideDuration={6000}>
            <Alert
              severity="success"
              onClose={() =>
                this.setState({ UserAddedSnackbar: false }, onClose)
              }
            >
              המשתמש נוצר בהצלחה
            </Alert>
          </Snackbar>

          <Snackbar
            open={this.state.FormErrorsSnackbar}
            autoHideDuration={6000}
          >
            <Alert
              severity="error"
              onClose={() => this.setState({ FormErrorsSnackbar: false })}
            >
              {this.state.errList}
            </Alert>
          </Snackbar>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(AddUserDialog);
