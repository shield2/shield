import React from "react";
import axios from "axios";
import { styles } from "./UsersStyle.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  Button,
  withStyles,
} from "@material-ui/core";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class DeleteUserDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      FirstName: this.props.FirstName,
      LastName: this.props.LastName,
      Id: this.props.Id,
      UserHasCoursesSnackBar: false,
      UserDeleteErrorSnackBar: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.Id !== this.props.Id) {
      this.setState({
        FirstName: this.props.FirstName,
        LastName: this.props.LastName,
        Id: this.props.Id,
        UserHasCoursesSnackBar: false,
        UserDeleteErrorSnackBar: false
      });
    }
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;

    const deleteUser = () => {
      var data = {
        id: this.state.Id,
      };

      axios
        .post(`/api/deleteUser`, data, {
          withCredentials: true,
        })
        .then((res) => {
          // console.log("success");
          this.setState({ FirstName: "", LastName: "", Id: "", IsAdmin: false });
          onClose();
        })
        .catch((err) => {
          // console.log("error")
          if (err.response.status === 403) {
            this.setState({ UserHasCoursesSnackBar: true });
          } else {
            this.setState({ UserDeleteErrorSnackBar: true });
          }
        });


    };

    return (
      <div className={classes.root}>
        <Dialog
          className={classes.dialog}
          open={isOpen}
          onClose={onClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"אישור מחיקה"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              האם אתה בטוח שברצונך למחוק את המשתמש <br />
              {this.state.FirstName} {this.state.LastName} (תעודת זהות-{" "}
              {this.state.Id})?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={deleteUser} className={classes.deleteBtn}>
              מחיקה
            </Button>
            <Button onClick={onClose} className={classes.CancelBtn}>
              ביטול
            </Button>
          </DialogActions>

          <Snackbar open={this.state.UserHasCoursesSnackBar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() =>
                this.setState({ UserHasCoursesSnackBar: false }, onClose)
              }
            >
              המשתמש משוייך לקורסים
            </Alert>
          </Snackbar>
          <Snackbar open={this.state.UserDeleteErrorSnackBar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() =>
                this.setState({ UserDeleteErrorSnackBar: false }, onClose)
              }
            >
              התקבלה שגיאה במחיקת משתמש
            </Alert>
          </Snackbar>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(DeleteUserDialog);
