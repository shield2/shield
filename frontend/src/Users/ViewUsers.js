import React from "react";
import { styles } from "./UsersStyle.css.js";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import axios from 'axios';
import { Fab, Tooltip, withStyles } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import AddUserDialog from "./AddUser.js"
import EditUserDialog from "./EditUser.js"
import DeleteUserDialog from "./DeleteUser.js"


class ViewUsers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      allUsers: [],
      addUserDialogOpen: false, // New user dialog open state
      editUserDialogOpen: false, // Delete user dialog open state
      DeleteUserDialogOpen: false, // Edit user dialog open state

      chosenUserId: "",
      chosenUserFirstName: "",
      chosenUserLastName: "",
      chosenUserIsAdmin: "",
      chosenUserMail: "",
    };
  }

  componentDidMount() {
    this.getUsers();
  }

  getUsers() {
    // console.log('in getUsers')
    axios.get(`/api/allUsers`, { withCredentials: true }).then(res => {
      this.setState({ allUsers: res.data })
      // console.log(res.data)
      // console.log("get users")
    }).catch(err => {
      // console.log("get courses error")
    })
  }

  render() {
    const { classes } = this.props;
    this.getUsers.bind(this)

    const handleAddUserOpen = () => {
      this.setState({ addUserDialogOpen: true });
    };

    const handleAddUserClose = () => {
      this.setState({ addUserDialogOpen: false });
      this.getUsers();
    };

    const handleEditUserOpen = (uID, uFirstName, uLastName, uIsAdmin, uMail) => {
      this.setState({
        chosenUserId: uID,
        chosenUserFirstName: uFirstName,
        chosenUserLastName: uLastName,
        chosenUserIsAdmin: uIsAdmin,
        chosenUserMail: uMail
      });

      this.setState({ editUserDialogOpen: true });
    };

    const handleEditUserClose = () => {
      this.setState({ editUserDialogOpen: false });
      this.setState({
        chosenUserId: "",
        chosenUserFirstName: "",
        chosenUserLastName: "",
        chosenUserIsAdmin: "",
        chosenUserMail: ""
      });

      this.getUsers();
    };

    const handleDeleteUserOpen = (uID, uFirstName, uLastName) => {
      this.setState({
        chosenUserId: uID,
        chosenUserFirstName: uFirstName,
        chosenUserLastName: uLastName,
      });

      this.setState({ DeleteUserDialogOpen: true });
    };

    const handleDeleteUserClose = () => {
      this.setState({ DeleteUserDialogOpen: false });
      this.getUsers();
    };

    // const readCookie = name => {
    //   var nameEQ = name + "=";
    //   var ca = document.cookie.split(';');
    //   for (var i = 0; i < ca.length; i++) {
    //     var c = ca[i];
    //     while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    //     if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    //   }
    //   return null;
    // }

    return (
      <div className={classes.root}>
        <div>
          <h1 className={classes.mainTitle}> משתמשי המערכת</h1>
          <Tooltip title="הוספת משתמש" aria-label="add">
            <Fab
              size="small"
              className={classes.btnAdd}
              aria-label="add"
              onClick={handleAddUserOpen}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        </div>

        <TableContainer component={Paper}>
          <Table size="small">
            <TableHead>
              <TableRow background="#eeeeee">
                <TableCell align="right">מספר זהות</TableCell>
                <TableCell align="right">שם פרטי</TableCell>
                <TableCell align="right">שם משפחה</TableCell>
                <TableCell align="right">כתובת מייל</TableCell>
                <TableCell align="right">האם מנהל?</TableCell>
                <TableCell align="right">עריכה</TableCell>
                <TableCell align="right">מחיקה</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.allUsers.map(u => (
                <TableRow key={u.id}>
                  <TableCell align="right">{u.id}</TableCell>
                  <TableCell align="right">{u.firstName}</TableCell>
                  <TableCell align="right">{u.lastName}</TableCell>
                  <TableCell align="right">{u.mail}</TableCell>
                  <TableCell align="right">{u.isAdmin ? "כן" : "לא"}</TableCell>
                  <TableCell align="right">
                    <Fab
                      size="small"
                      className={classes.editBtn}
                      aria-label="edit"
                      onClick={() => handleEditUserOpen(u.id, u.firstName, u.lastName, u.isAdmin, u.mail)}
                    >
                      <EditIcon />
                    </Fab>
                  </TableCell>
                  <TableCell align="right">
                    <Fab
                      size="small"
                      className={classes.deleteBtn}
                      aria-label="delete"
                      onClick={() => handleDeleteUserOpen(u.id, u.firstName, u.lastName)}
                    >
                      <DeleteIcon />
                    </Fab>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>

        <AddUserDialog open={this.state.addUserDialogOpen} onClose={handleAddUserClose} />
        <EditUserDialog
          open={this.state.editUserDialogOpen}
          onClose={handleEditUserClose}
          FirstName={this.state.chosenUserFirstName}
          LastName={this.state.chosenUserLastName}
          Id={this.state.chosenUserId}
          IsAdmin={this.state.chosenUserIsAdmin}
          Mail={this.state.chosenUserMail ? this.state.chosenUserMail : ""}
        />

        <DeleteUserDialog
          open={this.state.DeleteUserDialogOpen}
          onClose={handleDeleteUserClose}
          FirstName={this.state.chosenUserFirstName}
          LastName={this.state.chosenUserLastName}
          Id={this.state.chosenUserId}
        />
      </div>
    );
  }
}

export default withStyles(styles)(ViewUsers);
