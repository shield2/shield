export const styles = () => ({
    root: {
        direction: "rtl",
        margin: "5%",
        marginRight: 210,
        flexGrow: 1,
        textAlign: "right"
    },

    mainTitle: {
        direction: "rtl",
        textAlign: "right",
        textShadow: "2px 2px #b5c5b9",
        marginBottom: "30px"
    },

    editBtn: {
        backgroundColor: "#00bfa5",
        color: "white",
        margin: 5,
    },

    CancelBtn: {
        backgroundColor: "#9e9e9e",
        color: "white",
        margin: 5,
    },

    deleteBtn: {
        backgroundColor: "#d8626d",
        color: "white",
        margin: 5,
    },

    text: {
        textAlign: "right",
        direction: "rtl",
        marginRight: "15%"
    },

    btnAdd: {
        backgroundColor: "#4d94ff",
        color: "white",
        marginLeft: 1200,
        marginTop: 20,
        marginBottom: 20
    },


    // thead: {
    //     fontWeight: "1000 !important"
    // },

    dialog: {
        direction: "rtl",
        textAlign: "right",
        position: "relative",
        width: "70% !important",
        height: "80% !important",
        top: "10% !important",
        left: "20% !important"
    },

    dialogLable: {
        direction: "rtl",
        textAlign: "left",
        width: "100px !important",
        paddingLeft: "8px",
        display: "inline-block",
        height: "100%",
        verticalAlign: "middle",
        margin: "8px 0 4px"
    },
})
