import React from "react";
import axios from "axios";
import { styles } from "./UsersStyle.css.js";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Form } from "react-bootstrap";
import {
  Button,
  TextField,
  Checkbox,
  withStyles,
} from "@material-ui/core";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class EditUserDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      FirstName: this.props.FirstName,
      LastName: this.props.LastName,
      Id: this.props.Id,
      IsAdmin: this.props.IsAdmin,
      mail: this.props.Mail,
      errList: "",
      FormErrorsSnackbar: false
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.Id !== this.props.Id) {
      this.setState({
        FirstName: this.props.FirstName,
        LastName: this.props.LastName,
        Id: this.props.Id,
        IsAdmin: this.props.IsAdmin,
        mail: this.props.Mail,
      });
    }
  }

  render() {
    const { classes } = this.props;
    const isOpen = this.props.open;
    const onClose = this.props.onClose;
    var mailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const handleChanged = (e) => {
      this.setState({ [e.target.name]: e.target.value });
    };

    const handleChecked = (e) => {
      this.setState({ [e.target.name]: e.target.checked });
    };

    const editUser = () => {
      var data = {
        firstName: this.state.FirstName,
        lastName: this.state.LastName,
        id: this.state.Id,
        isAdmin: this.state.IsAdmin,
        mail: this.state.mail,
      };

      var errors = [];
      if (data.firstName === "") errors.push("לא הוזן שם פרטי");
      if (data.lastName === "") errors.push("לא הוזן שם משפחה");
      if (data.mail === "") errors.push("לא הוזנה כתובת מייל");
      else if (!mailRegex.test(data.mail)) errors.push("כתובת מייל לא תקינה");

      if (errors.length === 0) {
        axios
          .post(`/api/updateUser`, data, {
            withCredentials: true,
          })
          .then((res) => {
            // console.log("success");
            this.setState({
              FirstName: "",
              LastName: "",
              Id: "",
              IsAdmin: false,
              mail: "",
            }, onClose);
          })
          .catch((err) => {
            // console.log("error");
            this.setState({
              FirstName: "",
              LastName: "",
              Id: "",
              IsAdmin: false,
              mail: "",
            }, onClose);
          });
      } else {
        this.setState({ errList: errors.join(", "), FormErrorsSnackbar: true })
      }
    };

    return (
      <div className={classes.root}>
        <Dialog
          open={isOpen}
          onClose={onClose}
          aria-labelledby="form-dialog-title"
          className={classes.dialog}
        >
          <DialogTitle id="form-dialog-title">עריכת משתמש קיים</DialogTitle>
          <DialogContent>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>שם פרטי :</Form.Label>
              <TextField
                autoFocus
                margin="dense"
                type="text"
                name="FirstName"
                value={this.state.FirstName}
                onChange={handleChanged}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>
                שם משפחה :
              </Form.Label>
              <TextField
                autoFocus
                margin="dense"
                type="text"
                name="LastName"
                value={this.state.LastName}
                onChange={handleChanged}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>
                תעודת זהות:
              </Form.Label>
              <TextField
                autoFocus
                disabled
                margin="dense"
                type="text"
                name="Id"
                value={this.state.Id}
                onChange={handleChanged}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>
                כתובת מייל:
              </Form.Label>
              <TextField
                autoFocus
                margin="dense"
                type="text"
                name="mail"
                value={this.state.mail}
                onChange={handleChanged}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label className={classes.dialogLable}>האם מרצה?</Form.Label>
              <Checkbox
                onChange={handleChecked}
                checked={this.state.IsAdmin}
                name="IsAdmin"
                color="primary"
                inputProps={{ "aria-label": "secondary checkbox" }}
              />
            </Form.Row>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.setState({
              FirstName: "",
              LastName: "",
              Id: "",
              IsAdmin: false,
              mail: "",
            }, onClose)} color="primary">
              ביטול
            </Button>
            <Button onClick={editUser} color="primary">
              שמירה
            </Button>
          </DialogActions>
          <Snackbar open={this.state.FormErrorsSnackbar} autoHideDuration={6000}>
            <Alert
              severity="error"
              onClose={() =>
                this.setState({ FormErrorsSnackbar: false })
              }
            >
              {this.state.errList}
            </Alert>
          </Snackbar>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(EditUserDialog);
