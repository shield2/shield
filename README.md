# shield

This project manage your shield for the projects in the university/college.


## How to start
to start the whole project (frontend and backend) just be in /root folder and follow the command:

`npm start`

if you want to run only one of them /backend or /frontend, you sould enter the specific folder and run the command:

`npm start`

## How to install - npm
the same for `npm run install-all` for the entire project or be specific for one of the sides if you want to. for example:

`cd frontend/`

`npm install`


## DevOps
This procces will make a docker-image to run the entire project on a server.
